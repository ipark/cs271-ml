import numpy as np
import random
import matplotlib.pyplot as plt

def generator(iters):
    X0s = []; X1s = []; Cs = []
    print("(X0,  X1) class|  Y   | w0,w1,w2,w3,w4,w5")
    while iters:
        iters -= 1
        # Input 
        X0 = random.random() #random.choice([1,0])
        X1 = random.random() #random.choice([1,0])
        # Weights 
        w0 = random.choice([1,-1])
        w1 = random.choice([1,-1])
        w2 = random.choice([1,-1])
        w3 = random.choice([1,-1])
        w4 = random.choice([1,-1])
        w5 = random.choice([1,-1])
        Ws=[w0,w1,w2,w3,w4,w5]
    
        Y = w4 * max(w0 * X0 + w2 * X1, 0) + w5 * max(w1 * X0 + w3 * X1, 0)
    
        ##if Y == 0 and (X0 == X1): # XOR=0
        if Y >= 0 and Y < 0.5:    # (X0,X1) classified as 0
            print(f"({X0:.2f},{X1:.2f})=> 0| {Y:.2f} | {w0:2d},{w1:2d},{w2:2d},{w3:2d}{w4:2d},{w5:2d}")
            X0s.append(X0)
            X1s.append(X1)
            Cs.append(0)
        elif Y >= 0.5 and Y <= 1: # (X0,X1) classified as 1
            print(f"({X0:.2f},{X1:.2f})=> 1| {Y:.2f} | {w0:2d},{w1:2d},{w2:2d},{w3:2d}{w4:2d},{w5:2d}")
            X0s.append(X0)
            X1s.append(X1)
            Cs.append(1)
    return X0s, X1s, Cs

if __name__ == "__main__":
    iters = 100
    X0s, X1s, Cs = generator(iters)
    for i in range(len(X0s)):
        x=X0s[i]; y=X1s[i]; c=Cs[i]
        if Cs[i] == 0:
            #plt.scatter(x, y, color='r', alpha=0.5, marker='o')
            plt.text(x, y, str(c), color="red", fontsize=12)
        else:
            #plt.scatter(x, y, color='b', alpha=0.5, marker='o')
            plt.text(x, y, str(c), color="blue", fontsize=12)
    plt.axhline(0.5,  linestyle='--') # horizontal lines
    plt.axvline(0.5,  linestyle='--') # vertical lines
    plt.grid(which='minor', alpha=0.5)
    plt.xlabel('X0')
    plt.ylabel('X1')
    plt.show()
