#!/usr/bin/env python3

import sys # for sys.argv
import itertools # read line ranges
import os.path # exists

# mapping the observation symbols to index
obsSymbols = ' abcdefghijklmnopqrstuvwxyz'
obsSymbols2Index = {}
for index,character in enumerate(obsSymbols):
    obsSymbols2Index[character] = index

from math import ceil
def selPartOfText(fileName, startCol, T, skip):
    lines = []
    # sel by row range                                                             
    startLine = ceil(skip/40)
    endLine = ceil((T+skip)/40)
    print("In py: Sel Text from Lines:%d--%d" % (startLine, endLine))
    with open(fileName, 'r') as f:
        for line in itertools.islice(f, startLine, endLine):
            lines.append(line[startCol:])
    f.close()
    return lines

def getObsSeq_TnSkip(fileName, startCol, T, skip):
    #print (f"In py: get observations sequence...skip-chars{skip}")
    # read file and select part of text
    lines = selPartOfText(fileName, startCol, T, skip)
    # convert to lower-case and keep only 26 characters and a word space
    obs_seq = []
    for line in lines:
        # remove leading spaces, trimming newline and converting lowercase
        line = (line.lstrip()).rstrip().lower()
        for c in line:
            if c.isspace() or c.isalpha():
                obs_seq.append(obsSymbols2Index[c]) # convert char to number
    # set maximum T in case read in extra lines from line
    if len(obs_seq) > T:
        obs_seq = obs_seq[:T]
    T = len(obs_seq)
    M = len(set(obs_seq))
    print(f"In py: T={T}")
    print(f"In py: M={M}")
    O = ' '.join([str(x) for x in obs_seq])
    with open("T.out", "w") as f:
        f.write("%d %d" % (T, M))
    with open("O.out", "w") as f:
        f.write(O)

def main():
    # make sure "old" files are removed for c not to read "old" ones
    import os
    if os.path.isfile("T.out"):
        os.remove("T.out")
    if os.path.isfile("O.out"):
        os.remove("O.out")

    if len(sys.argv) != 5:
        print("""
        USAGE:
        getObsSeq_TnSkip.py textFile  startCol  T     skip 
                            [1]       [2]       [3]   [4] 
        1. textFile:   file name to be read/analyzed (only 1 file)
        2. startCol:   starting column number to be read in
        3. T:          observation sequence
        4. skip:       starting to get seq after skip over "skip" chars
        """)
        exit(1)
    else:
        fileName = sys.argv[1]       
        startCol = int(sys.argv[2])
        T = int(sys.argv[3])
        skip = int(sys.argv[4])

        """
        1. Given
        Observation sequence O = (O_0, O_1, . . . , O_{T-1}).
        """
        getObsSeq_TnSkip(fileName, startCol, T, skip)

if __name__ == '__main__':
    main()

