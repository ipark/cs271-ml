#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

/**
 * CS271 - Fall2019 - Inhee Park 
 * HMMQ10ga.c (HMM using Gradient Ascent instead of Hill Climb)
 * gcc -o hmmQ10ga -Wall -O3 hmmQ10ga.c
 */
#define N           2 
#define T        5000
#define TMAX    50000
#define M          27
#define MAX_ITER  500
#define K           5 // random fluctuation (1/N/K)

float pi[N];
int O[T]; 
float c[TMAX]; // c_t
float alpha[TMAX][N];
float beta[TMAX][N]; 
float Gamma[TMAX][N];
float diGamma[TMAX - 1][N][N]; 

float tau = 2.5;   // temperature factor 
float rho = 12.0f; // learning rate

// Deep Learning chapter A.4 Table.1
float w[N][N] = { // initialization
    {2.f, 2.f},
    {1.f, 2.f}
};
// Deep Learning chapter A.4 Table.2
float v[N][M] = { // initialization
    {2, 1, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 1, 2, 2, 2, 1, 1, 1, 2, 1, 1, 2, 1, 2, 2},
    {2, 1, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 2, 2, 1, 2, 1, 2, 1, 2, 1, 1, 1, 2, 2, 1},
};

// Deep Learning chapter A.2 Equation(8) on a_ij
float a[N][N]; // a_ij
void set_a()
{
    for (int i = 0; i < N; ++i) {
        // set probability of a[i][] such that \sum_{j=0}^{N-1} a[i][j] = 1
        float s = 0.f;
        for (int k = 0; k < N; ++k) {
            s += expf(tau * w[i][k]);
        }
        for (int j = 0; j < N; ++j) {
            a[i][j] = exp(tau * w[i][j]) / s;
        }
        // assert \sum_{j=0}^{N-1} a[i][j] ~ 1
        s = 0.f;
        for (int j = 0; j < N; ++j) {
            s += a[i][j];
        }
    }
}

// Deep Learning chapter A.2 Equation(8) on b_i(j)
float b[N][M]; // b_i(j)
void set_b()
{
    for (int i = 0; i < N; ++i) {
        // set probability of b[i][] such that \sum_{j=0}^{M-1} b[i][j] = 1
        float s = 0.f;
        for (int k = 0; k < M; ++k) {
            s += expf(tau * v[i][k]);
        }
        for (int j = 0; j < M; ++j) {
            b[i][j] = exp(tau * v[i][j]) / s;
        }
    }
}


/**
  set probabilities, p[i] for i = 0, 1, ..., n-1 such that
    0) p[i] ~ p0 +/- f
    1) 0 <= p[i] < 1
    2) sum of p[i] = 1
 */

void set_probabilities(float *p, int n, float p0, float f)
{
    float sum = 0.f;
//  printf("p0 = %f; f = %f\n", p0, f);
    for (int i = 0; i < n; ++i) {
        float r = rand() / (float) RAND_MAX; // [0, RAND_MAX] -> [0, 1]
//      printf("r = %f\n", r);
        r = 2 * f * r - f; // [0, 2f] -> [-f, f]
//      printf("r = %f; f = %f\n", r, f);
//      printf("p0 + r = %.3f + (%.3f)\n", p0, r);
        p[i] = p0 + r;
        sum += p0 + r;
    }
    // normalize
    for (int i = 0; i < n; ++i) {
        p[i] /= sum;
    }
}

void initLambdaModel()
{
    srand(time(NULL));
    float p0 = (float) 1 / N;

    set_probabilities(pi, N, p0, p0 / K);

    for (int i = 0; i < N; ++i) {
        set_probabilities(a[i], N, p0, p0 / K);
    }

    printf("M=%d\n", M);
    //printf("K=%d\n", K);
    float q0 = (float) 1 / M;
    for (int i = 0; i < N; ++i) {
        set_probabilities(b[i], M, q0, q0 / K);
    }
}


void alphaPass(void) {
    c[0] = 0;
    for (int i = 0; i < N; ++i) {
        alpha[0][i] = pi[i] * b[i][O[0]];
        c[0] += alpha[0][i];
    }

    c[0] = 1 / c[0];
    for (int i = 0; i < N; ++i) {
        alpha[0][i] *= c[0];
    }
    for (int t = 1; t < T; ++t) {
        c[t] = 0;
        for (int i = 0; i < N; ++i) {
            alpha[t][i] = 0;
            for (int j = 0; j < N; ++j) {
                alpha[t][i] += alpha[t - 1][j] * a[j][i];
            }
            alpha[t][i] *= b[i][O[t]];
            c[t] += alpha[t][i];
        }
        c[t] = 1 / c[t];
        for (int i = 0; i < N; ++i) {
            alpha[t][i] *= c[t];
        }
    }
}

void betaPass(void) {
    for (int i = 0; i < N; ++i) {
        beta[T - 1][i] = c[T - 1];
    }

    for (int t = T - 2; t >= 0; --t) {
        for (int i = 0; i < N; ++i) {
            beta[t][i] = 0;
            for (int j = 0; j < N; ++j) {
                beta[t][i] += a[i][j] * b[j][O[t + 1]] * beta[t + 1][j];
            }
            beta[t][i] *= c[t];
        }
    }
}

void GammasPass(void) {
    for (int t = 0; t < T - 1; ++t) {
        float denom = 0;
        for (int i = 0; i < N; ++i) {
            for (int j = 0; j < N; ++j) {
                denom += alpha[t][i] * a[i][j] * b[j][O[t + 1]] * beta[t + 1][j];
            }
        }
        for (int i = 0; i < N; ++i) {
            Gamma[t][i] = 0;
            for (int j = 0; j < N; ++j) {
                diGamma[t][i][j] = alpha[t][i] * a[i][j] * b[j][O[t + 1]] * beta[t + 1][j] / denom;
                Gamma[t][i] += diGamma[t][i][j];
            }
        }
    }
    float denom = 0;
    for (int i = 0; i < N; ++i) {
        denom += alpha[T - 1][i];
    }
    for (int i = 0; i < N; ++i) {
        Gamma[T - 1][i] = alpha[T - 1][i] / denom;
    }
}

void reEstimateModel(float log_ct)
{
/*
    // re-estimate pi
    for i = 0 to N-1
        pi_i = gamma_0(i)
    next i
 */
    for (int i = 0; i < N; ++i) {
        pi[i] = Gamma[0][i];
    }
/*
    // re-estimate a and b
    for i = 0 to N-1
        denom = 0
        for t = 0 to T-2
            denom = denom + gamma_t(i)
        next t
        for j = 0 to N-1 // re-estimate a
            numer = 0
            for t = 0 to T-2
                numer = numer + digamma_t(i,j)
            next t
            aij = numer/denom
        next j
        for j = 0 to M-1 // re-estimate b
            numer = 0
            for t = 0 to T-2
                if(O_t == j) then
                    numer = numer + gamma_t(i)
            next t
            bi(j) = numer/denom
        next j
    next i
 */
/* this is Hill-climb version of re-estimate 
    for (int i = 0; i < N; ++i) {
        float denom = 0.f;
        for (int t = 0; t < T - 1; ++t) {
            denom += Gamma[t][i];
        }
        for (int j = 0; j < N; ++j) {
            float numer = 0.f;
            for (int t = 0; t < T - 1; ++t) {
                numer += diGamma[t][i][j];
            }
            a[i][j] = numer / denom;
        }
        for (int j = 0; j < M; ++j) {
            float numer = 0.f;
            for (int t = 0; t < T - 1; ++t) {
                if (O[t] == j) {
                    numer += Gamma[t][i];
                }
            }
            b[i][j] = numer / denom;
        }
    }
*/
    // This is a Gradient Ascent Version of
    // Re-estimate of model parameters
    ///////////////////////////////
    // NEW to GA:  A_ij(O), A_i(O)
    // Deep Learning Chapter A.2 Equation(11)
    ///////////////////////////////
    // A_ij(O) - A_i(O) * a_ij
    for (int i = 0; i < N; ++i) {
        float AiO = 0.f;
        for (int t = 0; t < T - 1; ++t) {
            AiO += Gamma[t][i];
        }
        for (int j = 0; j < N; ++j) {
            float AijO = 0.f;
            for (int t = 0; t < T - 1; ++t) {
                AijO += diGamma[t][i][j];
            }
            float dij = AijO - AiO * a[i][j];
            /*
              Actually, C(O) = 1/\Pi_{t = 0}^{T - 1} c_t.
              But in A.2 Equaiton(16) says 
              C(O) = \sum_{t = 0}^{T - 1} log(c_t) for denominator
             */
            w[i][j] += rho * dij / log_ct; // A.2 Equaiton(9)
        }
    } // EO-A's-w_ij
    ///////////////////////////////
    // NEW to GA:  B_ij(O), B_i(O) 
    // Deep Learning Chapter A.2 Equation(11)
    ///////////////////////////////
    // B_ij(O) - B_i(O) * b_i(j)
    for (int i = 0; i < N; ++i) {
        float BiO = 0.f;
        for (int t = 0; t < T; ++t) {
            BiO += Gamma[t][i];
        }
        for (int j = 0; j < M; ++j) {
            float BijO = 0.f;
            for (int t = 0; t < T - 1; ++t) { 
                if (O[t] == j) {
                    BijO += Gamma[t][i];
                }
            }
            float dij = BijO - BiO * b[i][j];
            /*
              Actually, C(O) = 1/\Pi_{t = 0}^{T - 1} c_t.
              But in A.2 Equaiton(16) says 
              C(O) = \sum_{t = 0}^{T - 1} log(c_t) for denominator
             */
            v[i][j] += rho * dij / log_ct;
        }
    } // EO-B's-vij
} // EO-reEstimateModel

void set_O(int skip) 
{
    ///////////////////////////////                                                   
    // 1. Given
    // Observation sequence O = (O_0, O_1, . . . , O_{T-1}).
    ///////////////////////////////
    //  concatenate some of input argument to execute python code for text processing
    //  to obtain an observation sequence in numbers (0, 1, ..., 26)
    //int skip = T * epoch;
    char cmd[100]; 
    sprintf(cmd, "python getObsSeqO_TnSkip.py corpus_all 15 %d %d", T, skip);
    // python getObsSeqO_TnSkip.py corpus_all 15 5000 10000
    //printf("%s\n", cmd);
    
    // read O.out file spitted out from python execution observation seq in numbers
    // convert the read-in number sequences to array O
    FILE *f;
    f = fopen("O.out", "r");
    // int O[T];
    int Oi = 0, n = 0;
    while(fscanf(f, "%d", &Oi) > 0) {
        O[n++] = Oi;
    }
    fclose(f);
}


int main(int argc, char *argv[]) {

    ///////////////////////////////
    // Initialize three matrices a, b, and pi.
    // let pi_i ~ 1/N and let a_ij ~ 1/N and b_j(k) ~ 1/M.
    // verify the row stochastic conditions
    ///////////////////////////////
    initLambdaModel();

    ///////////////////////////////
    // NEW to GA: use epochs for "online" update
    // upon augmented new observatioal new data
    ///////////////////////////////
    for (int epoch = 0; epoch < TMAX / T; ++epoch) {
        printf("\nEpoch=%d\n", epoch);
        set_O(T * epoch);

        for (int iter = 0; iter < MAX_ITER; ++iter) {
            //////////////////////////////////                  
            // NEW to GA: set a_ij and b_i(j)
            //////////////////////////////////
            set_a();
            set_b();
                                                                
            ///////////////////////////////
            // 3. Forward algorithm or alpha-pass
            ///////////////////////////////
            alphaPass();
                                                                
            ///////////////////////////////
            // 4. Backward algorithm or beta-pass
            ///////////////////////////////
            betaPass();
                                                                
            ///////////////////////////////
            // 5. Compute the Gammas and di-Gammas
            ///////////////////////////////
            GammasPass();
                                                                
            ///////////////////////////////
            // 6. Compute log P(O|lambda)
            ///////////////////////////////
            float log_ct = 0.f;
            for (int t = 0; t < T; ++t) {
                log_ct += logf(c[t]); // from alphaPass
            }
                                                                
            ///////////////////////////////
            // 7. Re-estimate model parameters
            //  NEW to GA:  A_ij(O), A_i(O), B_ij(O) and B_i(O)
            ///////////////////////////////
            reEstimateModel(log_ct);

        } // EO-for-iteration

        if (epoch % 2 == 0) {
            printf("--B^T (ecoch=%d)--\n", epoch);                           
            float b0 = 0.f;
            float b1 = 0.f;
            for (int j = 0; j < M; ++j) {
                if (j == 0) {
                    printf("%2c   %.5f %.5f\n", j, b[0][j], b[1][j]);
                } else {
                    printf("%c   %.5f %.5f\n", 96+j, b[0][j], b[1][j]);
                }
                b0 +=  b[0][j];
                b1 +=  b[1][j];
            }
            printf("---------------------\n");
            printf("sum: %.1f\t%.1f\n\n", b0, b1);
        }
    } // EO-for-epochs

    printf("--Final B matrix-----\n");
    float b0 = 0.f;
    float b1 = 0.f;
    for (int j = 0; j < M; ++j) {
        if (j == 0) {
            printf("%2c   %.5f %.5f\n", j, b[0][j], b[1][j]);
        } else {
            printf("%c   %.5f %.5f\n", 96+j, b[0][j], b[1][j]);
        }
        b0 +=  b[0][j];
        b1 +=  b[1][j];
    }
    printf("---------------------\n");
    printf("sum: %.1f\t%.1f\n", b0, b1);

    return 0;
} // EO-main()
