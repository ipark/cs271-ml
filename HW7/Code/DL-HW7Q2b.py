import itertools
import random

XOR00 = []
XOR01 = []
XOR10 = []
XOR11 = []

iters = 100
while iters:
    # Input 
    X0 = random.choice([1,0])
    X1 = random.choice([1,0])
    # Weights 
    w0 = random.choice([1,-1])
    w1 = random.choice([1,-1])
    w2 = random.choice([1,-1])
    w3 = random.choice([1,-1])
    w4 = random.choice([1,-1])
    w5 = random.choice([1,-1])
    
    Y = w4 * max(w0 * X0 + w2 * X1, 0) + w5 * max(w1 * X0 + w3 * X1, 0)

    if Y == 0 and (X0 == X1 == 0): # XOR=0
        str_ = f"{X0:2d} {X1:2d} | {Y:2d} | {w0:2d},{w1:2d},{w2:2d},{w3:2d},{w4:2d},{w5:2d}"
        XOR00.append(str_)
    elif Y == 0 and (X0 == X1 == 1): # XOR=0
        str_ = f"{X0:2d} {X1:2d} | {Y:2d} | {w0:2d},{w1:2d},{w2:2d},{w3:2d},{w4:2d},{w5:2d}"
        XOR11.append(str_)
    elif Y == 1 and (X0 == 0 and X1 == 1): # XOR=1
        str_ = f"{X0:2d} {X1:2d} | {Y:2d} | {w0:2d},{w1:2d},{w2:2d},{w3:2d},{w4:2d},{w5:2d}"
        XOR01.append(str_)
    elif Y == 1 and (X0 == 1 and X1 == 0): # XOR=1
        str_ = f"{X0:2d} {X1:2d} | {Y:2d} | {w0:2d},{w1:2d},{w2:2d},{w3:2d},{w4:2d},{w5:2d}"
        XOR10.append(str_)
    iters -= 1

print("X0 X1 |  Y | w0,w1,w2,w3,w4,w5")
print("------+----+------------------")
for _ in XOR00:
    print(_)
print("------+----+------------------")
for _ in XOR01:
    print(_)
print("------+----+------------------")
for _ in XOR10:
    print(_)
print("------+----+------------------")
for _ in XOR11:
    print(_)
print("------+----+------------------")
