"""
CS271-HW7-DL-Q6
"""
from math import exp, floor

class XOR_SGD():
    def __init__(self):
        pass

    def forwardPass(self, X0, X1, Z, backpropagation):
        # initial weights
        w0, w1, w2, w3, w4, w5 = self.Ws

        v0 = w0 # initialization #1
        v1 = w1 # initialization #2
        v2 = w2 # initialization #3
        v3 = w3 # initialization #4
        v4 = w4 # initialization #5
        v5 = w5 # initialization #6
        v6 = X0 * v0 + X1 * v2   #7 
        v7 = X0 * v1 + X1 * v3   #8
        v8 = 1 + exp(-v6)        #9
        v9 = 1 + exp(-v7)        #10
        v10 = v4 / v8            #11
        v11 = v5 / v9            #12
        v12 = 0.5*(v10 + v11 - Z)**2 #13
        z = v12                 # 14, which is E(w)

        if backpropagation: # page.23 of DL chapter
            self.Vs = v4, v5, v6, v7, v8, v9, v10, v11, v12
            return z  # E(w)
        else: # for inference only
            return v10 + v11 # which is Y

    def backwardPass(self, X0, X1, Z, learningRate):
        v4, v5, v6, v7, v8, v9, v10, v11, v12 = self.Vs

        dz = 1
        dv11 = v10 + v11 - Z
        dv10 = v10 + v11 - Z
        dv9 = -(v5 / v9**2) * dv11
        dv8 = -(v4 / v8**2) * dv10
        dv7 = -exp(-v7) * dv9
        dv6 = -exp(-v6) * dv8
        dv5 = dv11 / v9
        dv4 = dv10 / v8
        dv3 = X1 * dv7
        dv2 = X1 * dv6
        dv1 = X0 * dv7
        dv0 = X0 * dv6

        # now weights (wi_new = wi_old - learningRate * dvi)
        w0, w1, w2, w3, w4, w5 = self.Ws
        w0 -= learningRate * dv0
        w1 -= learningRate * dv1
        w2 -= learningRate * dv2
        w3 -= learningRate * dv3
        w4 -= learningRate * dv4
        w5 -= learningRate * dv5
        self.Ws = w0, w1, w2, w3, w4, w5

    def train(self, trainSet, learningRate, initWeights, epochs):
        """
        Y = w4/1+exp(-(w0*X0+w2*X1)) + w5/1+exp(-(w1*X0+w3*X1))
        """
        self.Ws = initWeights # w0, w1, w2, w3, w4, w5
        for i in range(epochs):
            for X0, X1, Z in trainSet:
                Ew = self.forwardPass(X0, X1, Z, backpropagation=True)
                self.backwardPass(X0, X1, Z, learningRate)

    def inference(self, dataSet):
        matches = 0
        for X0, X1, Z0 in dataSet:
            Y = self.forwardPass(X0, X1, Z0, backpropagation=False)
            if Y < 0.5:
                Z = 0
                print(f"(X0={X0},X1={X1}; Z0={Z0})<->(Z={Z}; Y={Y:.2f} < 0.5)")
            else:
                Z = 1
                print(f"(X0={X0},X1={X1}; Z0={Z0})<->(Z={Z}; Y={Y:.2f} >= 0.5)")
            if Z == Z0:
                matches += 1
        print(f"accuracy = {matches}/{len(dataSet)}")

def main():
    trainSet = [ #X0, X1, Z
                (0.6, 0.4, 1), #0
                (0.1, 0.2, 0), #1
                (0.8, 0.6, 0), #2
                (0.3, 0.7, 1), #3
                (0.7, 0.3, 1), #4
                (0.7, 0.7, 0), #5
                (0.2, 0.9, 1)  #6
                ]
    testSet =  [ #X0, X1, Z
                 (0.55, 0.11, 1), #0
                 (0.32, 0.21, 0), #1
                 (0.24, 0.64, 1), #2
                 (0.86, 0.68, 0), #3
                 (0.53, 0.79, 0), #4
                 (0.46, 0.54, 1), #5
                 (0.16, 0.51, 1), #6
                 (0.52, 0.94, 0), #7
                 (0.46, 0.87, 1), #8
                 (0.96, 0.63, 0)  #9
                 ]
    model = XOR_SGD()
    learningRate = 0.1
    initWeights = 1, 2, -1, 1, -2, 1
    # Q6b
    epochs = 1000
    model.train(trainSet, learningRate, initWeights, epochs)
    print("inference on the train set, epochs=1K")
    model.inference(trainSet)
    print("inference on the test set, epochs=1K")
    model.inference(testSet)
    # Q6b
    epochs = 10000
    model.train(trainSet, learningRate, initWeights, epochs)
    print("inference on the train set, epochs=10K")
    model.inference(trainSet)
    print("inference on the test set, epochs=10K")
    model.inference(testSet)

if __name__ == '__main__':
    main()
