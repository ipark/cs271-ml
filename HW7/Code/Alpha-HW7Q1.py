# HW7-Alpha-Q1-CNN
import pprint
from math import floor

def conv(A, F):
    N = len(A) # input pixel dimension
    M = len(F) # filter dimension
    # output matrix B (N-M+1)x(N-M+1) dimension
    B = [[0 for j in range(N - M + 1)] for i in range(N - M + 1)]

    for i in range(N - M + 1):
        for j in range(N - M + 1):
            B[i][j] = 0 # initialization
            for I in range(M):
                for J in range(M):
                    B[i][j] += A[i + I][j + J] * F[I][J]

    return B

def pooling(A, F):
    N = len(A) # input pixels dimension
    P = floor(N/F) # pooling output dimension
    # output matrix B PxP dimension
    B = [[0 for j in range(P)] for i in range(P)]
    for i in range(P):
        for j in range(P):
            pools = []
            for I in range(F):
                for J in range(F):
                    pools.append(A[F*i + I][F*j + J])
            B[i][j] = max(pools)
    return B

if __name__ == "__main__":
    pixels = [
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0],
            [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
            [0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0],
            [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
            [0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0],
            [0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0],
            [0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0],
            [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    ]

    # Q1a
    # Fig2(d) kernel
    anti_diagonal = [
            [-1, -1, 2],
            [-1, 2, -1],
            [2, -1, -1]
    ]
    anti_diagonal_B = conv(pixels, anti_diagonal)
    #pprint.pprint(anti_diagonal_B)

    # Q1b
    # Fig2(a) kernel
    diagonal = [
            [2, -1, -1],
            [-1, 2, -1],
            [-1, -1, 2],
    ]
    diagonal_B = conv(pixels, diagonal) # this is Fig3-right-hand-side matrix
    print("Before 4x4 pooling")
    pprint.pprint(diagonal_B)
    print("After 4x4 pooling")
    pprint.pprint(pooling(diagonal_B, 4))

