# python3
import numpy as np
from scipy import linalg

class PCA:
    def __init__(self, B, UDV=None):
        """
        B = (X1, X2, ..., Xm): matrix of m column vectors
        Xi: column vector of length = n

        B: m x n matrix
        """
        m, n = B.shape

        self.mu = np.mean(B, axis=1) # vector of length = m
        A = B - self.mu.reshape((-1, 1)) # per each column; done by "broadcast"
        self.B, self.A = B, A
        # covariance matrix
        self.C = A @ A.transpose() / n

        if UDV: # Sec. 4.6
            U, D, V = UDV
            S = np.zeros((U.shape[1], V.shape[1]))
            for i in range(min(S.shape)):
                S[i, i] = D[i]

            # verify A = U S V^T
            #print(np.allclose(A, U @ S @ V.transpose()))

            self.U, self.D, self.S, self.V = U, D, S, V
            self.score_matrix = U.transpose() @ A
            return

        U, D, VT = linalg.svd(A)
        # A: m x n matrix
        # U: m x m matrix
        # D: vector of length = min(m, n)
        # VT: n x n matrix; V^T
        V = VT.transpose()
        S = np.zeros(A.shape)
        for i in range(min(A.shape)):
            S[i, i] = D[i]

        # verify A = U S V^T
        #print(np.allclose(A, U @ S @ VT))

        self.U, self.D, self.S, self.V = U, D, S, V

        self.eigenvectors = np.transpose(U, (1, 0))
        self.eigenvalues = D * D / n

        for i in range(min(m, n)):
            e = self.eigenvalues[i]
            v = self.eigenvectors[i]
#           print(e, v)
#           print(np.allclose(self.C @ v, e * v))

        self.score_matrix = U.transpose() @ A
        self.score_vectors = np.transpose(self.score_matrix, (1, 0))

    def score(self, Y, N=-1):
        Y1 = Y - self.mu

        W = Y1 @ self.U[:, :N] # vector of length = N

        sm = self.score_matrix[:N, :]
        m, n = self.score_matrix.shape
        dist = list()
        for j in range(n):
            dist.append(np.linalg.norm(W - sm[:, j]))
        return min(dist)


def prob10(pca):
    s1=pca.score(np.array([2, 3, 1, 0, 3, 2]).transpose(), N=2)
    s2=pca.score(np.array([-4, -5, 0, 3, 1, -2]).transpose(), N=2)
    s3=pca.score(np.array([2, 3, 0, 1, 3, 2]).transpose(), N=2)
    s4=pca.score(np.array([3, 2, 1, 0, 3, 2]).transpose(), N=2)
    print("Y1^T=[2, 3, 1, 0, 3, 2]; score=%.4f" % s1)
    print("Y2^T=[-4, -5, 0, 3, 1, -2]; score=%.4f" % s2)
    print("Y3^T=[2, 3, 0, 1, 3, 2]; score=%.4f" % s3)
    print("Y4^T=[3, 2, 1, 0, 3, 2]; score=%.4f" % s4)

def sec4_6():
    print("Q10 (Sec. 4.6)")
    B = np.array(
        [
            [ 2, 1, 0, 3, 1, 1], # X1^T
            [ 2, 3, 1, 2, 3, 0], # X2^T
            [ 1, 0, 3, 3, 1, 1], # X3^T
            [ 2, 3, 1, 0, 3, 2]  # X4^T
           ],
           dtype=np.float
    ).transpose() # B = (X1, X2, X3, X4)
    U = np.array(
        [
            [ 0.1641,  0.2443, -0.0710],
            [ 0.6278,  0.1070,  0.2934],
            [-0.2604, -0.8017,  0.3952],
            [-0.5389,  0.4277,  0.3439],
            [ 0.4637, -0.1373,  0.3644],
            [ 0.0752, -0.2904, -0.7083]
        ]
    )
    D = np.array(
        [4.0414, 2.2239, 1.7237]
    )
    V = np.array(
        [
            [-0.2739,  0.6961, -0.4364],
            [ 0.3166,  0.2466,  0.7674],
            [-0.6631, -0.5434,  0.1224],
            [ 0.6205, -0.3993, -0.4534]
        ]
    )
    pca4_6 = PCA(B, (U,D,V))
    prob10(pca4_6)


if __name__ == "__main__":
    sec4_6()
