# python3
import numpy as np
from scipy import linalg

class PCA:
    def __init__(self, B, UDV=None):
        """
        B = (X1, X2, ..., Xm): matrix of m column vectors
        Xi: column vector of length = n

        B: m x n matrix
        """
        m, n = B.shape

        self.mu = np.mean(B, axis=1) # vector of length = m
        A = B - self.mu.reshape((-1, 1)) # per each column; done by "broadcast"
        print("Q11a")
        print("A="); print(A)
        self.B, self.A = B, A
        # covariance matrix
        self.C = A @ A.transpose() / n

        if UDV: # Sec. 4.6
            U, D, V = UDV
            S = np.zeros((U.shape[1], V.shape[1]))
            for i in range(min(S.shape)):
                S[i, i] = D[i]

            # verify A = U S V^T
            #print(np.allclose(A, U @ S @ V.transpose()))

            self.U, self.D, self.S, self.V = U, D, S, V
            self.score_matrix = U.transpose() @ A
            return

        U, D, VT = linalg.svd(A)
        # A: m x n matrix
        # U: m x m matrix
        # D: vector of length = min(m, n)
        # VT: n x n matrix; V^T
        V = VT.transpose()
        S = np.zeros(A.shape)
        for i in range(min(A.shape)):
            S[i, i] = D[i]

        # verify A = U S V^T
        #print(np.allclose(A, U @ S @ VT))

        self.U, self.D, self.S, self.V = U, D, S, V

        self.eigenvectors = np.transpose(U, (1, 0))
        self.eigenvalues = D * D / n

        print("\nEigenval [EivenVec]=")
        for i in range(min(m, n)):
            e = self.eigenvalues[i]
            v = self.eigenvectors[i]
            print(e, v)
#           print(np.allclose(self.C @ v, e * v))

        self.score_matrix = U.transpose() @ A
        print("\nScoring Matrix =")
        print(U.transpose() @ A)
        self.score_vectors = np.transpose(self.score_matrix, (1, 0))

    def score(self, Y, N=-1):
        Y1 = Y - self.mu

        W = Y1 @ self.U[:, :N] # vector of length = N

        sm = self.score_matrix[:N, :]
        m, n = self.score_matrix.shape
        dist = list()
        for j in range(n):
            dist.append(np.linalg.norm(W - sm[:, j]))
        return min(dist)

def prob11():
    B = np.array(
        [
            [ 2,-1, 0, 1, 1,-3, 5, 2], # X1^T
            [-2, 3, 2, 3, 0, 2,-1, 1], # X2^T
            [-1, 3, 3, 1,-1, 4, 5, 2], # X3^T
            [ 3,-1, 0, 3, 2,-1, 3, 0]  # X4^T
        ],
        dtype=np.float
    ).transpose() # B = (X1, X2, X3, X4)
    pca = PCA(B)
    print("\nQ11b\n scores of Y's")
    print("Y1^T=[ 1, 5, 1, 5, 5, 1, 1, 3] score %.4f" % pca.score(np.array([ 1, 5, 1, 5, 5, 1, 1, 3]).transpose(), N=3))
    print("Y2^T=[-2, 3, 2, 3, 0, 2,-1, 1] score %.4f" % pca.score(np.array([-2, 3, 2, 3, 0, 2,-1, 1]).transpose(), N=3))
    print("Y3^T=[ 2,-3, 2, 3, 0, 0, 2,-1] score %.4f" % pca.score(np.array([ 2,-3, 2, 3, 0, 0, 2,-1]).transpose(), N=3))
    print("Y4^T=[ 2,-2, 2, 2,-1, 1, 2, 2] score %.4f" % pca.score(np.array([ 2,-2, 2, 2,-1, 1, 2, 2]).transpose(), N=3))


if __name__ == "__main__":
    prob11()
