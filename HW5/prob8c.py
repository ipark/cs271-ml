import numpy as np
def prob8c(A, U, V):
    COL = U.shape[1]
    u = np.hsplit(U, COL)
    v = np.hsplit(V, COL)
    for j in range(COL): 
        print(f"v{j+1}^T")
        uj = u[j]
        vj = v[j]
        print(vj.transpose())
        Avj = A @ vj
        print(f"(A v{j+1})^T")
        print(Avj.transpose())
        print(f"(A v{j+1})^T / ||A v{j+1}||")
        Avj = Avj / (Avj.transpose() @ Avj)**.5 
        print(Avj.transpose())
        print(f"u{j+1}^T")
        print(uj.transpose())

A = np.array(
    [
        [1./4, 1./4, -3./4, 1./4],
        [-3./4, 5./4, -7./4, 5./4],
        [-5./4, -1./4, 7./4, -1./4],
        [1, 0, 1, -2],
        [-1, 1, -1, 1],
        [0, -1, 0, 1]
    ]
)

U = np.array(
    [
        [0.1641, 0.2443, -0.0710],
        [0.6278, 0.1070, 0.2934],
        [-0.2604, -0.8017, 0.3952],
        [-0.5389, 0.4277, 0.3439],
        [0.4637, -0.1373, 0.3644],
        [0.0753, -0.2904, -0.7083]
    ]
)

V = np.array(
    [
        [-0.2789, 0.6961, -0.4564], 
        [0.3166, 0.2466, 0.7674], 
        [-0.6631, -0.5434, 0.1224],
        [0.6205, -0.3993, -0.4534]
    ]
)

print("Q8c")  
prob8c(A, U, V)
