# python3
import numpy as np
from scipy import linalg

class PCA:
    def __init__(self, B, UDV=None):
        """
        B = (X1, X2, ..., Xm): matrix of m column vectors
        Xi: column vector of length = n

        B: m x n matrix
        """
        m, n = B.shape

        self.mu = np.mean(B, axis=1) # vector of length = m
        A = B - self.mu.reshape((-1, 1)) # per each column; done by "broadcast"
        self.B, self.A = B, A
        # covariance matrix
        self.C = A @ A.transpose() / n

        if UDV: # Sec. 4.6
            U, D, V = UDV
            S = np.zeros((U.shape[1], V.shape[1]))
            for i in range(min(S.shape)):
                S[i, i] = D[i]

            # verify A = U S V^T
            #print(np.allclose(A, U @ S @ V.transpose()))

            self.U, self.D, self.S, self.V = U, D, S, V
            self.score_matrix = U.transpose() @ A
            return

        U, D, VT = linalg.svd(A)
        # A: m x n matrix
        # U: m x m matrix
        # D: vector of length = min(m, n)
        # VT: n x n matrix; V^T
        V = VT.transpose()
        S = np.zeros(A.shape)
        for i in range(min(A.shape)):
            S[i, i] = D[i]

        # verify A = U S V^T
        #print(np.allclose(A, U @ S @ VT))

        self.U, self.D, self.S, self.V = U, D, S, V

        self.eigenvectors = np.transpose(U, (1, 0))
        self.eigenvalues = D * D / n

        for i in range(min(m, n)):
            e = self.eigenvalues[i]
            v = self.eigenvectors[i]
#           print(e, v)
#           print(np.allclose(self.C @ v, e * v))

        self.score_matrix = U.transpose() @ A
        print (U.transpose() @ A)
        self.score_vectors = np.transpose(self.score_matrix, (1, 0))

    def score(self, Y, N=-1):
        Y1 = Y - self.mu

        W = Y1 @ self.U[:, :N] # vector of length = N

        sm = self.score_matrix[:N, :]
        m, n = self.score_matrix.shape
        dist = list()
        for j in range(n):
            dist.append(np.linalg.norm(W - sm[:, j]))
        return min(dist)


def sec4_6():
    #print("Sec. 4.6:")
    B = np.array(
        [
            [-1, 2, 1, 2,-1, 0], # X1^T
            [-2, 1, 2, 3, 2, 1], # X2^T
            [-1, 3, 0, 1, 3,-1], # X3^T
            [ 0, 2, 3, 1, 1,-2]  # X4^T
           ],
           dtype=np.float
    ).transpose() # B = (X1, X2, X3, X4)
    U = np.array(
        [
            [ 0.1641,  0.2443, -0.0710],
            [ 0.6278,  0.1070,  0.2934],
            [-0.2604, -0.8017,  0.3952],
            [-0.5389,  0.4277,  0.3439],
            [ 0.4637, -0.1373,  0.3644],
            [ 0.0752, -0.2904, -0.7083]
        ]
    )
    D = np.array(
        [4.0414, 2.2239, 1.7237]
    )
    V = np.array(
        [
            [-0.2739,  0.6961, -0.4364],
            [ 0.3166,  0.2466,  0.7674],
            [-0.6631, -0.5434,  0.1224],
            [ 0.6205, -0.3993, -0.4534]
        ]
    )


def prob13():
    M = np.array(
        [
            [ 1,-1, 1,-1,-1, 1], # X1^T
            [-2, 2, 2,-1,-2, 2], # X2^T
            [ 1, 3, 0, 1, 3, 1], # X3^T
            [ 2, 3, 1, 1,-2, 0]  # X4^T
            ],
            dtype=np.float
    ).transpose() # M = (X1, X2, X3, X4)
    print ("Q13a. Scoring matrix on Malware")
    pcaM = PCA(M)
    B = np.array(
        [
            [-1, 2, 1, 2,-1, 0], # X1^T
            [-2, 1, 2, 3, 2, 1], # X2^T
            [-1, 3, 0, 1, 3,-1], # X3^T
            [ 0, 2, 3, 1, 1,-2]  # X4^T
           ],
           dtype=np.float
    ).transpose() # B = (X1, X2, X3, X4)
    print ("Q13b. Scoring matrix on Benigh")
    pcaB = PCA(B)

    Ys = [
        np.array([ 1, 5, 1, 5, 5, 1]).transpose(),
        np.array([-2, 3, 2, 3, 0, 2]).transpose(),
        np.array([ 2,-3, 2, 3, 0, 0]).transpose(),
        np.array([ 2,-2, 2, 2,-1, 1]).transpose(),
    ]
    print ("Q13c")
    for Y in Ys:
        sM = pcaM.score(Y, N=2)
        sB = pcaB.score(Y, N=2)
        print("Y=", Y, "; score M = %.2f, score B = %.2f" % (sM, sB))
        if sM < sB:
            print("...is \"malign\"")
        else:
            print("...is \"benign\"")

if __name__ == "__main__":
    sec4_6()
    prob13()
