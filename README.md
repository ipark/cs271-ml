CS271. Topics in Machine Learning (Fall2019 @SJSU)
=========

Textbook:  Introduction to Machine Learning with Applications in Information Security (1st ed.) Mark Stamp. 2017.
---------

Topics:
-------

Week 1 --- Introduction and overview

Week 2 --- Hidden Markov Models

Week 3 --- Data Analysis

Week 4 --- Applications of Hidden Markov Models

Week 5 --- Profile Hidden Markov Models

Week 6 --- Applications of Profile Hidden Markov Models

Week 7 --- Principal Component Analysis

Week 8 --- Applications of Principal Component Analysis

Week 9 --- Support Vector Machines

Week 10 --- Applications of Support Vector Machines

Week 11 --- Clustering

Week 12 --- Clustering Applications

Week 13 --- k-Nearest Neighbor, Neural Networks, Boosting/AdaBoost, Random Forests

Week 14 --- Linear Discriminant Analysis, Naive Bayes, Regression Analysis, Conditional Random Fields

Week 15 --- Project presentations

Project: Sentiment Analysis using Machine Learning and Deep Learning
=========

Abstract
--------
<blockquote>
"Sentiment Analysis" (positive, negative polarity classification on the social media or customer reviews) 
is chosen as a major topic in the project, because 1) it is a text-based classification task in Natural 
Language Processing (NLP); 2) it can be analysed by classifiers in Machine Learning such as Support 
Vector Machine (linear classifier) and Naive Bayesian Model (probabilistic classifier); 3) moreover, 
it can be explored by pre-trained state-of-art NLP model (BERT) from Transfer Learning. 
Our Sentiment Analysis results show that primitive TF-IDF with Naive Bayes (80%) outperforms the start-of-art BERT (75%). Perhaps depending on the specific NLP task, not always contextualized embedding is required to capture the simple polarity.
</blockquote>

Index Terms
-----------
Sentiment Analysis, Deep Learning, Natural Language Processing, Word Embeddings, Word2Vec, TF-IDF, BERT SVM, Naive Bayes, LDA

### Codes: [Colab Notebook](https://gitlab.com/ipark/cs271-ml/blob/master/MLproject/Code/CS271_SentimentAnalysis_InheePark.ipynb)

### Report: [Report](https://gitlab.com/ipark/cs271-ml/blob/master/MLproject/IEEE/CS271-Project-PARK.pdf)

### Presentation: [Slide](https://gitlab.com/ipark/cs271-ml/blob/master/MLproject/Presentation/CS271-SentimentAnalsysByMLDL-PARK.pdf)

