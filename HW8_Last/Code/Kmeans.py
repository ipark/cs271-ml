# CS271-ML-HW8 (Inhee Park)
# Ch6. Clustering-Q5
import copy

# compute central point of X=(duration, wait)
# per ClusterSet
def computeCentroid(X, ClusterSet):
    if len(ClusterSet) == 0:
        return 1e6, 1e6 # no cluster, then let inf
    sum_duration, sum_wait = 0, 0
    for indX in ClusterSet:
        duration, wait = X[indX]
        sum_duration += duration
        sum_wait += wait
    center_duration = sum_duration / len(ClusterSet)
    center_wait = sum_wait / len(ClusterSet)
    return center_duration, center_wait

# page 137 of the textbook
# Eq.(6.1) distorsion=\sum_{i=1}^n d(Xi, centroid(Xi))
#          distorsion=\sum_{i=1}^n dist2
def dist2(Xi, centroidXi):
    duration, wait = Xi
    centerDuration, centerWait = centroidXi
    return (centerDuration - duration)**2 + (centerWait - wait)**2

def Kmeans(X, K, NMAX, NUM_NO_CHANGE):
    #
    # Algorithm 6.2 K-means clustering
    #
    # 1. Given data points X1,...,Xn
    #    Number of clusters K
    # 2. Initialize:
    #    Partition X1,...,Xn into clusters C1,...,CK
    ClusterSet = list() # clusters
    for j in range(K):
        ClusterSet.append(set([]))
    # initialize by round robin
    i = 0
    for indX in range(len(X)):
        ClusterSet[i].add(indX)
        i += 1
        if i == K:
            i = 0  # reset and round again
    print("initial partition:")
    print(ClusterSet)

    #
    # 3. while stopping criteria is not met
    #    for j = 1 to K 
    #      let centroid cj be the center of cluster Cj
    #    NOTE THAT cj is element in centroids=[] 
    #    NOTE THAT Cj is element in ClusterSet=list(set([]))
    num_change = 0
    # NMAX : number of max iteration
    for i in range(NMAX):
        centroids = [] # every iteration, recompute centroids (cj's)
        for j in range(K):
            # compute central point of X=(duration, wait)
            # per ClusterSet
            centroids.append(computeCentroid(X, ClusterSet[j]))
        prev_ClusterSet = copy.copy(ClusterSet)

        # reset and make clusters...
        ClusterSet = []
        for j in range(K):
            ClusterSet.append(set([]))
        #
        # for i = 1 to n
        #   assign Xi to cluster Cj (ClusterSet) 
        #   so that d(Xi,cj)<=d(Xi,cl), l={1,...,K}
        for indX, (duration, wait) in enumerate(X):
            a = [] # list of (j, dist2)
            # per ClusterSet (Cj)
            for j in range(K):
                Xi = (duration, wait)
                centroid_Xi = centroids[j]
                a.append((j, dist2(Xi, centroid_Xi)))
            b = sorted(a, key=lambda e: e[1]) # sort by dist2
            closestCj = b[0][0] # index of cluster Cj closest to Xi
            ClusterSet[closestCj].add(indX)
        if ClusterSet == prev_ClusterSet:
            num_change += 1
        else:
            num_change = 0
        #print("iter=%d : NumChange/MaxTrial = %d/%d" % (i+1, num_change, NUM_NO_CHANGE))
        if num_change == NUM_NO_CHANGE:
            break
    return ClusterSet

if __name__ == "__main__":
    # on page 165 of the textbook
    # table 6.6: old faithful data
    duration = list()
    duration += [3.600, 1.800, 2.283, 3.333, 2.883, 4.533, 1.950, 1.833, 4.700, 3.600]
    duration += [1.600, 4.350, 3.917, 4.200, 1.750, 1.800, 4.700, 2.167, 4.800, 1.750]
    wait = list()
    wait += [79, 54, 62, 74, 55, 85, 51, 54, 88, 85]
    wait += [52, 85, 84, 78, 62, 51, 83, 52, 84, 47]
    X = []
    for d, w in zip(duration, wait):
        X.append((d, w))
    print("dataset")
    for i, xy in enumerate(X):
        print(i, xy)

    # Ch6-Q5-a) K=2
    NMAX_ITER=100
    K=2
    print("\n******* K(=%d)-Means Clustering *******" % K)
    ClusterSet = Kmeans(X, K, NMAX_ITER, NUM_NO_CHANGE=10)
    print("final clusters result:")
    for j in range(K):
        print("Cluster%d : %s" % (j, ClusterSet[j]))

    # Ch6-Q5-a) K=3
    K=3
    print("\n******* K(=%d)-Means Clustering *******" % K)
    ClusterSet = Kmeans(X, K, NMAX_ITER, NUM_NO_CHANGE=10)
    print("final clusters result:")
    for j in range(K):
        print("Cluster%d : %s" % (j, ClusterSet[j]))

    # K=4
    K=4
    print("\n******* K(=%d)-Means Clustering *******" % K)
    ClusterSet = Kmeans(X, K, NMAX_ITER, NUM_NO_CHANGE=10)
    print("final clusters result:")
    for j in range(K):
        print("Cluster%d : %s" % (j, ClusterSet[j]))
