# CS271-HW8-Inhee Park
# Ch6. Clustering - Q16. DBSCAN
# data is obtained from 
# http://www.cs.sjsu.edu/~stamp/ML/files/dbscan.txt
#
points = [
        (1.0,5.0),
        (1.25,5.35),
        (1.25,5.75),
        (1.5,6.25),
        (1.75,6.75),
        (2.0,6.5),
        (3.0,7.75),
        (3.5,8.25),
        (3.75,8.75),
        (3.95,9.1),
        (4.0,8.5),
        (2.5,7.25),
        (2.25,7.75),
        (2.0,6.5),
        (2.75,8.25),
        (4.5,8.9),
        (9.0,5.0),
        (8.75,5.85),
        (9.0,6.25),
        (8.0,7.0),
        (8.5,6.25),
        (8.5,6.75),
        (8.25,7.65),
        (7.0,8.25),
        (6.0,8.75),
        (5.5,8.25),
        (5.25,8.75),
        (4.9,8.75),
        (5.0,8.5),
        (7.5,7.75),
        (7.75,8.25),
        (6.75,8.0),
        (6.25,8.25),
        (4.5,8.9),
        (5.0,1.0),
        (1.25,4.65),
        (1.25,4.25),
        (1.5,3.75),
        (1.75,3.25),
        (2.0,3.5),
        (3.0,2.25),
        (3.5,1.75),
        (3.75,8.75),
        (3.95,0.9),
        (4.0,1.5),
        (2.5,2.75),
        (2.25,2.25),
        (2.0,3.5),
        (2.75,1.75),
        (4.5,1.1),
        (5.0,9.0),
        (8.75,5.15),
        (8.0,2.25),
        (8.25,3.0),
        (8.5,4.75),
        (8.5,4.25),
        (8.25,3.35),
        (7.0,1.75),
        (8.0,3.5),
        (6.0,1.25),
        (5.5,1.75),
        (5.25,1.25),
        (4.9,1.25),
        (5.0,1.5),
        (7.5,2.25),
        (7.75,2.75),
        (6.75,2.0),
        (6.25,1.75),
        (4.5,1.1),
        (3.0,4.5),
        (7.0,4.5),
        (5.0,3.0),
        (4.0,3.35),
        (6.0,3.35),
        (4.25,3.25),
        (5.75,3.25),
        (3.5,3.75),
        (6.5,3.75),
        (3.25,4.0),
        (6.75,4.0),
        (3.75,3.55),
        (6.25,3.55),
        (4.75,3.05),
        (5.25,3.05),
        (4.5,3.15),
        (5.5,3.15),
        (4.0,6.5),
        (4.0,6.75),
        (4.0,6.25),
        (3.75,6.5),
        (4.25,6.5),
        (4.25,6.75),
        (3.75,6.25),
        (6.0,6.5),
        (6.0,6.75),
        (6.0,6.25),
        (5.75,6.75),
        (5.75,6.25),
        (6.25,6.75),
        (6.25,6.25),
        (9.5,9.5),
        (2.5,9.5),
        (1.0,8.0)
]
#print(len(points))

# to be used for a key in neighbors_map dictionary data structure
point_key = lambda x, y: f'{x},{y}'

def set_neighbors(e):
    neighbors_map = dict()
    # all pair-wise distance calculation
    # O(N^2); not efficient
    for (x, y) in points:
        key = point_key(x, y)
        neighbors_map[key] = list()
        for (x1, y1) in points:
            if (x - x1)**2 + (y - y1)**2 < e**2:
                neighbors_map[key].append((x1, y1))
    return neighbors_map

#
# e (epsilon): reachable distance range
# m : min number of core point
def DBSCAN(e, m):
    '''https://en.wikipedia.org/wiki/DBSCAN'''
    neighbors_map = set_neighbors(e)
    label_map = dict()
    for x, y in points:
        key = point_key(x, y)
        label_map[key] = None

    clusters = list()

    ClustCounter = 0
    for p in points:
        p_key = point_key(p[0], p[1])
        if label_map[p_key] != None:
            continue
        neighbors = neighbors_map[p_key]
        # [outlier]
        # less number of points than a core point
        # let is outlier or noise
        if len(neighbors) < m:
            label_map[p_key] = -1 # outlier
            continue
        # [core point]
        # core point, i.e. len(neighbors) >= m
        p_cluster = list()
        label_map[p_key] = ClustCounter
        p_cluster.append(p)
        # queue or...
        Qs = list(neighbors)
        k = 0
        while k < len(Qs):
            q = Qs[k]
            k += 1
            if q == p: # skip itself
                continue
            q_key = point_key(q[0], q[1])
            if label_map[q_key] == -1: # not visited
                label_map[q_key] = ClustCounter
                p_cluster.append(q)
            if label_map[q_key] is not None: # already visited
                continue
            label_map[q_key] = ClustCounter
            p_cluster.append(q)

            q_neighbors = neighbors_map[q_key]
            if len(q_neighbors) >= m:
                for q1 in q_neighbors:
                    Qs.append(q1)

        clusters.append(p_cluster)

        ClustCounter += 1 # for next cluster
    return clusters, label_map

def print_clusters(e, m):
    print(f'e, m = {e}, {m}')
    clusters, label_map = DBSCAN(e, m)
    for ClustCounter, p_cluster in enumerate(clusters):
        print(f'C{ClustCounter}: {p_cluster}')

if __name__ == "__main__":
    for e, m in [(0.6, 3), (0.75, 4), (1.0, 5), (2.0, 10)]:
        print_clusters(e, m)
