# CS271-ML-HW8 (Inhee Park)
# Ch6. Clustering-Q11
import numpy as np
import math

def f(X, theta):
    X = X.reshape(2, 1)
    mu, S = theta
    #
    # on page 165, S is 2wait 
    # det(S) = s11*s22-s12*s21
    detS = S[0][0] * S[1][1] - S[0][1] * S[1][0]
    #
    # on page 165, 
    # inv(S)=(1/det(S))*[[s22, -s12]
    #                    [-s21, s11]]                               
    invS = (1/detS)* np.array([[S[1][1], -S[0][1]],\
                               [-S[1][0], S[0][0]]], dtype=S.dtype)
    #
    # Eq.(6.18)
    # f(X,theta)=(1/2*pi*sqrt(det(S)))*exp(-(1/2)*z)
    # z=(X-mu)^T*inv(S)*(X-mu)
    X_mu = X - mu
    z = X_mu.transpose() @ invS @ X_mu
    GaussianDist = math.exp(-z[0][0] / 2) / (2 * math.pi * math.sqrt(detS))
    return GaussianDist

def E_step(X, tau, theta):
    # 
    # on page 166
    # given our current estimates for tau, theta1, theta2,
    # use Bayes' formula to compute p_{j,i}
    # j=1,2 (let m)
    # i=1,2,...,n (let n)
    # p_{j,i} = tau_j f(X_i, theta_j) 
    #           --------------------------------------------
    #           tau_1 f(X_i, ehta_1) + tau_2 f(X_i, theta_2)
    n = X.shape[1]
    m = X.shape[0]
    p = np.empty(X.shape, dtype=X.dtype)
    for i in range(n):
        for j in range(m):
            Xi = X[:, i]
            p[j][i] = tau[j] * f(Xi, theta[j]) / \
                    (tau[0] * f(Xi, theta[0]) + tau[1] * f(Xi, theta[1]))
    return p

def M_step(X, p):
    # here in M step, we compute tau_j and theta_1 and theta_2
    n = X.shape[1]
    m = X.shape[0]

    # on page 167, Eq.(6.20)
    # \tau_j = \sum_{i=1}^n p_{j,i} / n
    tau = np.sum(p, axis=1) / n

    # on page 167, Eq.(6.21)
    # \muj = \sum_{i=1}^n X_i p_{j,i} / \sum_{i=1}^n p_{j,i}
    # where each X_i  is a vector, so compute a vector of means muj
    mu = X @ p.transpose() / np.sum(p, axis=1)

    # on page 167, Eq.(6.22)
    # Sj = \sum_{i=1}^n p_{j,i} (X_i - \muj) (X_i - \muj)^T / \sum_{i=1}^n p_{j,i}
    theta = list()
    for j in range(m):
        muj = mu[:, j] # (2,)
        Sj = np.zeros((2, 2), dtype=X.dtype)
        for i in range(n):
            Xi = X[:, i]
            Xi_muj = X[:, i] - mu[:, j]
            Xi_muj = Xi_muj.reshape(2, 1)
            Sj += p[j][i] * (Xi_muj @ Xi_muj.transpose())
        # here we need to divide Sj with \sum_{i=1}^n p_{j,i}
        # we already computed tau above
        # \tau_j = \sum_{i=1}^n p_{j,i} / n
        # thus \sum_{i=1}^n p_{j,i} = (n * \tau_j)
        Sj /= (n * tau[j])

        theta.append((muj.reshape(2, 1), Sj))
    return tau, theta

if __name__ == "__main__":
    # on page 165 of the textbook
    # table 6.6: old faithful data
    duration = list()
    duration += [3.600, 1.800, 2.283, 3.333, 2.883, 4.533, 1.950, 1.833, 4.700, 3.600]
    duration += [1.600, 4.350, 3.917, 4.200, 1.750, 1.800, 4.700, 2.167, 4.800, 1.750]
    n1 = len(duration)
    wait = list()
    wait += [79, 54, 62, 74, 55, 85, 51, 54, 88, 85]
    wait += [52, 85, 84, 78, 62, 51, 83, 52, 84, 47]
    n2 = len(wait)
    X = np.stack((duration, wait), axis=0)
    #print("old faithful data")
    #print(X.shape)

    # on page 167, Eq.(6.23)
    mu1 = np.array([2.5, 65.0]).reshape(2, 1)
    S1 = np.array([[1.0, 5.0], [5.0, 100.0]])
    # on page 167, Eq.(6.23)
    mu2 = np.array([3.5, 70.0]).reshape(2, 1)
    S2 = np.array([[2.0, 10.0], [10.0, 200.0]])
    # on page 166, theta=(mu S)
    theta = [(mu1, S1), (mu2, S2)]

    # on page 168, parameter initialization 
    # let tau = (tau1 tau2) = (0.6 0.4)
    tau = np.array([0.6, 0.4])

    Interations = 2
    for iteration in range(Interations):
        print("-----------------------------")
        print("Iteration=%d" % (iteration+1)) 
        print("-----------------------------")
        print("INPUT tau=", tau)
        print()
        print("INPUT theta1=", theta[0])
        print()
        print("INPUT theta2=", theta[1])
        print()
        if iteration > 0:
            print("INPUT p_{j,i}=")
            print(p)
            print()
        p = E_step(X, tau, theta)
        print("OUTPUT p_{j,i}=")
        print(p)
        print()
        tau, theta = M_step(X, p)
        print("OUTPUT theta1=", theta[0])
        print()
        print("OUTPUT theta2=", theta[1])
        print()

