import numpy as np

Table3_6 = """0  85 63 74   70  84 61 57 62 70
85 0  79 73   66  59 94 61 59 51
63 79 0  75   68  60 55 85 52 65
74 73 75 0    105 54 60 78 59 53
70 66 68 105  0   40 61 79 58 39
84 59 60 54   40  0  68 45 75 78
61 94 55 60   61  68 0  64 72 42
57 61 85 78   79  45 64 0  50 70
62 59 52 59   58  75 72 50 0  81
70 51 65 53   39  78 42 70 81 0"""

rows = Table3_6.split('\n')
MSA_scores = np.ndarray(shape=(len(rows),len(rows)), dtype=int)
for i, row in enumerate(rows):
    cols = row.split()
    for j, col in enumerate(cols):
        if i < j:
            MSA_scores[i, j] = col
        else:
            MSA_scores[i, j] = 0
            
sorted_MSA = np.sort(MSA_scores, axis=None)[-9:]
xs,ys = np.where(MSA_scores >= sorted_MSA[0])
for (x,y) in zip(xs, ys):
    print "(%d,%d)=%d" % (x+1, y+1, MSA_scores[x][y])

