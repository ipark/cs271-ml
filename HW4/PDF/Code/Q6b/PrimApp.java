import java.util.*;

public class PrimApp {

  public static void main(String[] args) {

    ////////////////////////////////////////////////
    // Ch3-Table3.6
    //0  85 63 74   70  84 61 57 62 70
    //85 0  79 73   66  59 94 61 59 51
    //63 79 0  75   68  60 55 85 52 65
    //74 73 75 0    105 54 60 78 59 53
    //70 66 68 105  0   40 61 79 58 39
    //84 59 60 54   40  0  68 45 75 78
    //61 94 55 60   61  68 0  64 72 42
    //57 61 85 78   79  45 64 0  50 70
    //62 59 52 59   58  75 72 50 0  81
    //70 51 65 53   39  78 42 70 81 06
    ////////////////////////////////////////////////
    String[] vertexList = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
    int numVertices = vertexList.length;
    List<Edge> edges = new ArrayList<>();
    edges.add(new Edge(0, 0, 0 )); 
    edges.add(new Edge(0, 1,85)); 
    edges.add(new Edge(0, 2,63)); 
    edges.add(new Edge(0, 3,74)); 
    edges.add(new Edge(0, 4,70)); 
    edges.add(new Edge(0, 5,84)); 
    edges.add(new Edge(0, 6,61)); 
    edges.add(new Edge(0, 7,57)); 
    edges.add(new Edge(0, 8,62)); 
    edges.add(new Edge(0, 9,70)); 
    //
    edges.add(new Edge(1, 0,85)); 
    edges.add(new Edge(1, 1,0 )); 
    edges.add(new Edge(1, 2,79)); 
    edges.add(new Edge(1, 3,73)); 
    edges.add(new Edge(1, 4,66)); 
    edges.add(new Edge(1, 5,59)); 
    edges.add(new Edge(1, 6,94)); 
    edges.add(new Edge(1, 7,61)); 
    edges.add(new Edge(1, 8,59)); 
    edges.add(new Edge(1, 9,51)); 
    //
    edges.add(new Edge(2, 0,63)); 
    edges.add(new Edge(2, 1,79)); 
    edges.add(new Edge(2, 2,0 )); 
    edges.add(new Edge(2, 3,75)); 
    edges.add(new Edge(2, 4,68)); 
    edges.add(new Edge(2, 5,60)); 
    edges.add(new Edge(2, 6,55)); 
    edges.add(new Edge(2, 7,85)); 
    edges.add(new Edge(2, 8,52)); 
    edges.add(new Edge(2, 9,65)); 
    //
    edges.add(new Edge(3, 0,74)); 
    edges.add(new Edge(3, 1,73)); 
    edges.add(new Edge(3, 2,75)); 
    edges.add(new Edge(3, 3,0 )); 
    edges.add(new Edge(3, 4,105)); 
    edges.add(new Edge(3, 5,54)); 
    edges.add(new Edge(3, 6,60)); 
    edges.add(new Edge(3, 7,78)); 
    edges.add(new Edge(3, 8,59)); 
    edges.add(new Edge(3, 9,53)); 
    //
    edges.add(new Edge(4, 0,70)); 
    edges.add(new Edge(4, 1,66)); 
    edges.add(new Edge(4, 2,68)); 
    edges.add(new Edge(4, 3,105)); 
    edges.add(new Edge(4, 4,0 )); 
    edges.add(new Edge(4, 5,40)); 
    edges.add(new Edge(4, 6,61)); 
    edges.add(new Edge(4, 7,79)); 
    edges.add(new Edge(4, 8,58)); 
    edges.add(new Edge(4, 9,39)); 
    //
    edges.add(new Edge(5, 0,84)); 
    edges.add(new Edge(5, 1,59)); 
    edges.add(new Edge(5, 2,60)); 
    edges.add(new Edge(5, 3,54)); 
    edges.add(new Edge(5, 4,40)); 
    edges.add(new Edge(5, 5,0 )); 
    edges.add(new Edge(5, 6,68)); 
    edges.add(new Edge(5, 7,45)); 
    edges.add(new Edge(5, 8,75)); 
    edges.add(new Edge(5, 9,78)); 
    //
    edges.add(new Edge(6, 0,61)); 
    edges.add(new Edge(6, 1,94)); 
    edges.add(new Edge(6, 2,55)); 
    edges.add(new Edge(6, 3,60)); 
    edges.add(new Edge(6, 4,61)); 
    edges.add(new Edge(6, 5,68)); 
    edges.add(new Edge(6, 6,0 )); 
    edges.add(new Edge(6, 7,64)); 
    edges.add(new Edge(6, 8,72)); 
    edges.add(new Edge(6, 9,42)); 
    //
    edges.add(new Edge(7, 0,57)); 
    edges.add(new Edge(7, 1,61)); 
    edges.add(new Edge(7, 2,85)); 
    edges.add(new Edge(7, 3,78)); 
    edges.add(new Edge(7, 4,79)); 
    edges.add(new Edge(7, 5,45)); 
    edges.add(new Edge(7, 6,64)); 
    edges.add(new Edge(7, 7,0 )); 
    edges.add(new Edge(7, 8,50)); 
    edges.add(new Edge(7, 9,70)); 
    //
    edges.add(new Edge(8, 0,62)); 
    edges.add(new Edge(8, 1,59)); 
    edges.add(new Edge(8, 2,52)); 
    edges.add(new Edge(8, 3,59)); 
    edges.add(new Edge(8, 4,58)); 
    edges.add(new Edge(8, 5,75)); 
    edges.add(new Edge(8, 6,72)); 
    edges.add(new Edge(8, 7,50)); 
    edges.add(new Edge(8, 8,0 )); 
    edges.add(new Edge(8, 9,81)); 
    //
    edges.add(new Edge(9, 0,70)); 
    edges.add(new Edge(9, 1,51)); 
    edges.add(new Edge(9, 2,65)); 
    edges.add(new Edge(9, 3,53)); 
    edges.add(new Edge(9, 4,39)); 
    edges.add(new Edge(9, 5,78)); 
    edges.add(new Edge(9, 6,42)); 
    edges.add(new Edge(9, 7,70)); 
    edges.add(new Edge(9, 8,81)); 
    edges.add(new Edge(9, 9,0)); 

    System.out.println("Minimum Spanning Tree by Prim's Algorithm");
    List<Edge> minSpanTree =  Prim.mst(edges, numVertices);
    for (Edge e : minSpanTree) 
      System.out.println(e);
  } // EO-main

}
