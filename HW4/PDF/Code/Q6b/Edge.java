import java.util.*;

/**
 * HW5-Q2.
 * In Kruskal's method uses PriorityQueue<Edge> to dequeue
 * the Edge with minimum distance.
 * Thus, Edge object should be comparable by implementing
 * Comparable<Edge> to override compareTo method
 * to be sorted by weight.
 */
public class Edge implements Comparable<Edge> {

  /** 
   * vertex v and w
   */
  private int v;
  private int w;
  private int weight;

  public Edge(int v, int w, int weight) {
    this.v = v;           
    this.w = w;
    this.weight = weight;
  }

  public int getv() {
    return v;
  }

  public int getw() {
    return w;
  }

  public int getWeight() {
    return weight;
  }

  /**
   * compare Edge by its weight
   */
  @Override
  public int compareTo(Edge otherEdge) {
    return this.weight == otherEdge.weight ? 0 : (
           this.weight > otherEdge.weight ? -1 : 1);
    //return 0;
  }

  /**
   * toString Edge
   */
  public String toString() {
    //System.out.println(vertexList[v]);
    int vSym = v + 1;
    int wSym = w + 1;
    return "(" + vSym + ", " + wSym + ") = " + weight;
  }
}
