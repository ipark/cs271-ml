"""
MSA=[
"EC----",
"EC-E-G",
"-CGEJG",
"EG--JG",
"EG---G"
]
"""
MSA=[
"A-AB-BAFCD-B-AAEA0ACEDA-EQ---A-ABCDBALF4-BBASB---AAAAFB",
"A-ABNBAFCD-B-AAEAABCEDA-EQ-CDABAB--BA-F4NBBM-BTYBAAAA--",
"A-AB-BAFCDAB-A-EAA-CEDCDEQA--ABFBAN---F4-BBAFBTYBAAAA--",
"2AAB-BAFCDAB-A-EAABCEDCDEQFCDABA-APAL-F4-BBA--SBAAAAA--",
"CDAB-BAFCDB1-AAEAA-CEDA-EQ-CDABABABAL-F4LBBAFBSBAAAAA--",
"CDABAAA----B-A-EA-ACEDCDEQ---A-ABCD-A-F4-BBASB---AAAAFB",
"CDAB--A-CDAB-A-EAA-CEDA-EQ-CDABCDCDAA-F4MBB--ATYBAAAA--",
"--AA-BA-CDB--AAEAA-CEDCDEQ-CDABPBA-AB-F4-BBAFBSBMAAAA--",
"CDAB--RBAFABPAAEA-ACEDCDEQAABCDAFAL---F4NBBASB---AAAAMB",
"A-ABAA-----B-AAEA-ACEDCDEQAABAFA------F4BNBASB---AAAAFB"
]

rows = len(MSA)
cols = len(MSA[0])
MSA_mat = [[0 for x in range(cols)] for y in range(rows)]
MSA_byCol = [0 for x in range(cols)]

def PHMM_Q3():
    symbols = {}
    for row, seq in enumerate(MSA):
        for col, c in enumerate(seq):
            MSA_mat[row][col] = c
            if c not in symbols.keys() and c != "-":
                symbols[c] = 1
    tot_symbols = len(symbols.keys())
    print tot_symbols, symbols.keys()
    Ms = 1
    Is = 0
    Icols = []
    for col in range(cols):
        #print str(col+1) + str(getCol(col))
        mydict = getColDist(col)
        if '-' in mydict.keys() and mydict['-'] > rows/2:
                print "\ncol%d: I%s-state" % (col+1, Is)                              
                Is += 1
                Icols.append(col)
                for sym in symbols:                                                
                    if sym in mydict.keys():
                        print " eI%d(%s)=%d/%d" % \
                        (Is, sym, mydict[sym] + 1, sum(mydict.values()) + tot_symbols),
                    else:
                        print " eI%d(%s)=%d/%d" % \
                        (Is, sym, 1, sum(mydict.values()) + tot_symbols),
        else:
            if '-' in mydict.keys():
                mydict['-'] = 0
            print "\ncol%d: M%d-state" % (int(col+1), Ms)
            Ms += 1
            for sym in symbols:
                if sym in mydict.keys():
                    print " eM%d(%s)=%d/%d" % \
                    (Ms, sym, mydict[sym] + 1, sum(mydict.values()) + tot_symbols),
                else:
                    print " eM%d(%s)=%d/%d" % \
                    (Ms, sym, 1, sum(mydict.values()) + tot_symbols),

    if len(Icols) > 1:
        print "\n========"
        mydict = getMultiColDist(Icols)                                    
        for sym in symbols:
            if sym in mydict.keys():
                print " eI(%s)=%d/%d" % \
                (sym, mydict[sym] + 1, sum(mydict.values()) + tot_symbols),
            else:
                print " eI(%s)=%d/%d" % \
                (sym, 1, sum(mydict.values()) + tot_symbols),

    


#def emission(state, col_seq, tot_symbols):
#    print "emission(%s)=%d/%d" % \
#            (state, + 1,  + tot_symbols)

def getColDist(col):
    col_dict = {}
    col_info = [row[col] for row in MSA_mat] 
    for state in col_info:
        if state in col_dict:
            count = col_dict[state]
            col_dict[state] = count + 1
        else:
            col_dict[state] = 1
    return col_dict

def getMultiColDist(col_list):
    col_dict = {}
    col_info = []
    for col in col_list:
        col_info.extend([row[col] for row in MSA_mat])
    for state in col_info:
        if state != '-':
            if state in col_dict:           
                count = col_dict[state]
                col_dict[state] = count + 1
            else:
                col_dict[state] = 1
    #print col_dict
    return col_dict
   
PHMM_Q3()

