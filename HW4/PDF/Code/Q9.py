MSA=[
"EC----",
"EC-E-G",
"-CGEJG",
"EG--JG",
"EG---G"
]

rows = len(MSA)
cols = len(MSA[0])
MSA_mat = [[0 for x in range(cols)] for y in range(rows)]
MSA_byCol = [0 for x in range(cols)]

def PHMM_Q3():
    symbols = {}
    for row, seq in enumerate(MSA):
        for col, c in enumerate(seq):
            MSA_mat[row][col] = c
            if c not in symbols.keys() and c != "-":
                symbols[c] = 1
    tot_symbols = len(symbols.keys())
    print tot_symbols, symbols.keys()
    Ms = 1
    Is = 0
    Icols = []
    for col in range(cols):
        #print str(col+1) + str(getCol(col))
        mydict = getColDist(col)
        if '-' in mydict.keys() and mydict['-'] > rows/2:
                print "\ncol%d: I%s-state" % (col+1, Is)                              
                Icols.append(col)
                for sym in symbols:                                                
                    print " eI%d(%s)=(0+1)/(0+%d)= 1/%d" % \
                    (Is, sym, tot_symbols, tot_symbols)
                Is += 1
        else:
            print "\ncol%d: M%d-state" % (int(col+1), Ms)
            if '-' in mydict.keys():
                mydict['-'] = 0
            for sym in symbols:
                if sym in mydict.keys():
                    print " eM%d(%s)=(%d+1)/(%d+%d)= %d/%d" % \
                    (Ms, sym, mydict[sym], sum(mydict.values()), tot_symbols, \
                    mydict[sym] + 1, sum(mydict.values()) + tot_symbols)
                else:
                    print " eM%d(%s)=(0+1)/(%d+%d)= %d/%d" % \
                    (Ms, sym, sum(mydict.values()), tot_symbols, \
                    1, sum(mydict.values()) + tot_symbols)
            Ms += 1


    if len(Icols) > 1:
        print "\n========"
        mydict = getMultiColDist(Icols)                                    
        for sym in symbols:
            if sym in mydict.keys():
                print " eI(%s)=(%d+1)/(%d+%d)=%d/%d" % \
                (sym, mydict[sym], sum(mydict.values()), tot_symbols, \
                mydict[sym] + 1, sum(mydict.values()) + tot_symbols)
            else:
                print " eI(%s)=(0+1)/(%d+%d)=1/%d" % \
                (sym, sum(mydict.values()), tot_symbols, \
                        sum(mydict.values()) + tot_symbols)

    


#def emission(state, col_seq, tot_symbols):
#    print "emission(%s)=%d/%d" % \
#            (state, + 1,  + tot_symbols)

def getColDist(col):
    col_dict = {}
    col_info = [row[col] for row in MSA_mat] 
    for state in col_info:
        if state in col_dict:
            count = col_dict[state]
            col_dict[state] = count + 1
        else:
            col_dict[state] = 1
    return col_dict

def getMultiColDist(col_list):
    col_dict = {}
    col_info = []
    for col in col_list:
        col_info.extend([row[col] for row in MSA_mat])
    for state in col_info:
        if state != '-':
            if state in col_dict:           
                count = col_dict[state]
                col_dict[state] = count + 1
            else:
                col_dict[state] = 1
    #print col_dict
    return col_dict
   
PHMM_Q3()

