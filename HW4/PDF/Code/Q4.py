# Ref: http://darlinglab.org/tutorials/alignment.py

#  E  G  C J
#E 9 -4  2 2
#G -4 9 -5 -5
#C 2 -5  10 7
#J 2 -5  7 10

import numpy as np

#seq_1 = "EJG"
#seq_2 = "GEECG"

subst_matrix = {
'E': {'E': 9,'G':-4,'C': 2,'J': 2},
'G': {'E':-4,'G': 9,'C':-5,'J':-5},
'C': {'E': 2,'G':-5,'C':10,'J': 7},
'J': {'E': 2,'G':-5,'C': 7,'J':10},
}
gap_penalty = -3

def MSA_DP(seq_1, seq_2, seq1Num, seq2Num):
    # this will store our dynamic programming matrix
    dp_matrix = np.ndarray(shape=(len(seq_1)+1,len(seq_2)+1), dtype=int)
    dp_matrix.fill(0)
    
    # storage for back pointers
    # 0 for up, 1 for diag, 2 for left
    back_ptr = np.ndarray(shape=(len(seq_1)+1,len(seq_2)+1), dtype=int)
    back_ptr.fill(-9)
    
    # fill the DP matrix
    best_score = 0
    for i in range(len(seq_1)+1):
        for j in range(len(seq_2)+1):
            if i==0 and j==0: continue # skip the first cell
    
            char_1 = seq_1[i-1] # current character at seq 1
            char_2 = seq_2[j-1] # current character at seq 2
    
            #
            # YOUR TASK: 'scores' must be filled in here
            # HINTS:                                                                         	
    	    #  * refer to the previous (e.g. diagonal predecessor) cell as dp_matrix[i-1,j-1]
    	    #  * get the substitution score as subst_matrix[char_1][char_2]
                #  * the gap score is called gap_penalty (these variables are defined above)
    	    #
            scores = [-999,-999,-999]
            if( i>0 and j>0 ):
                # score diagonal
                scores[1] = dp_matrix[i-1,j-1] + subst_matrix[char_1][char_2]
                #pass
            if( i>0 ):
                # score up: gap in sequence 2
                scores[2] = dp_matrix[i-1,j] + gap_penalty
                #pass
            if( j>0 ):
                # score left: gap in sequence 1
                scores[0] = dp_matrix[i,j-1] + gap_penalty
                #pass
    
            # select the best previous cell
            best = max(scores)
            dp_matrix[i,j]=best
            best_score += best
            for k in range(3):
                if scores[k] == best:
                    back_ptr[i,j] = k
    
    #print "best score = %d" % best_score
    #print("\nBack pointers:")
    #print(back_ptr)
    
    # read out the backtrace to get the best alignment
    aln_1 = ""
    aln_2 = ""
    i=len(seq_1)
    j=len(seq_2)
    
    while i>0 or j>0:
        if back_ptr[i,j] == 0: # left
            aln_1 += "-"
            aln_2 += seq_2[j-1]
            j -= 1
        if back_ptr[i,j] == 1: # diag
            aln_1 += seq_1[i-1]
            aln_2 += seq_2[j-1]
            i -= 1
            j -= 1
        if back_ptr[i,j] == 2: # up
            aln_1 += seq_1[i-1]
            aln_2 += "-"
            i -= 1
    
    aln_1 = aln_1[::-1] # reverses the string
    aln_2 = aln_2[::-1]
    
    print "---------------------------------"
    print("Seq%d:Seq%d Alignment:\n" % (seq1Num+1, seq2Num+1))
    print(aln_1)
    print(aln_2)
    print("\nDP matrix:")
    print(dp_matrix)
    return best_score
    
def main():
    seqs = ["EJG", "GEECG", "CGJEE", "JJGECCG"]
    MSA_scores = np.ndarray(shape=(len(seqs),len(seqs)), dtype=int)
    for i in range(len(seqs)):
        for j in range(len(seqs)):
            if i == j: 
                MSA_scores[i][j] = 0
            else:
                best_score = MSA_DP(seqs[i], seqs[j], i, j)
                MSA_scores[i][j] = best_score
    print "================================="
    print "Pairwise (global) Alignment Score"
    print MSA_scores

if __name__ == '__main__':
    main()
