import java.util.*;

public class Prim {

  public static List<Edge> mst(List<Edge> edges, int numVertices) {
    DisjointSets ds = new DisjointSets(numVertices);
    PriorityQueue<Edge> pq = new PriorityQueue<>(edges);
    List<Edge> minSpanTree = new ArrayList<>();

    while (minSpanTree.size() != numVertices - 1) {
      //Edge e = pq.deleteMin(); // Edge e = (v, w);
      Edge e = pq.remove(); // Edge e = (v, w);
      int vset = ds.find(e.getv());
      int wset = ds.find(e.getw());

      if (vset != wset) {
        // accept the edge
        minSpanTree.add(e);
        ds.union(vset, wset);
      } 
    } // EO-while
    return minSpanTree;
  }
}
