CS271 Project: Sentiment Analysis using Machine Learning and Deep Learning
=========

Abstract
--------
<blockquote>
"Sentiment Analysis" (positive, negative polarity classification on the social media or customer reviews) 
is chosen as a major topic in the project, because 1) it is a text-based classification task in Natural 
Language Processing (NLP); 2) it can be analysed by classifiers in Machine Learning such as Support 
Vector Machine (linear classifier) and Naive Bayesian Model (probabilistic classifier); 3) moreover, 
it can be explored by pre-trained state-of-art NLP model (BERT) from Transfer Learning. 
Our Sentiment Analysis results show that primitive TF-IDF with Naive Bayes (80%) outperforms the start-of-art BERT (75%). Perhaps depending on the specific NLP task, not always contextualized embedding is required to capture the simple polarity.
</blockquote>

Index Terms
-----------
Sentiment Analysis, Deep Learning, Natural Language Processing, Word Embeddings, Word2Vec, TF-IDF, BERT SVM, Naive Bayes, LDA

### Codes: [Colab Notebook](https://gitlab.com/ipark/cs271-ml/blob/master/MLproject/Code/CS271_SentimentAnalysis_InheePark.ipynb)

### Report: [Report](https://gitlab.com/ipark/cs271-ml/blob/master/MLproject/IEEE/CS271-Project-PARK.pdf)

### Presentation: [Slide](https://gitlab.com/ipark/cs271-ml/blob/master/MLproject/Presentation/CS271-SentimentAnalsysByMLDL-PARK.pdf)
