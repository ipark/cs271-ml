"""
CS271 - Fall2019 - HW1 - #10 - Inhee Park (7405)

INPUT English text
For the homework 1 assignment, use the Brown Corpus as your source of English text:
http://www.cs.toronto.edu/~gpenn/csc401/a1res.html
I then concate them all into one file.
ipark@DL:~/iparkDL/CS271_ML/HW1/corpus$ for x in `ls [A-R]*`; do cat  $x; done >> corpus_all

For problem 10 you must use HMM code that you have written entirely 
on your own, following the algorithms given in your textbook. 
Also, on problem 10 use M=27, that is, the 26 letters and word space 
(convert all upper-case letters to lower-case, and remove any symbols 
other than letters and word space).

Also, I'd prefer that your program use command line arguments, so that 
it is easy to select different parts of the text, different lengths of text, 
and so on. You need to provide a readme file that explains how to compile 
and execute your code. In any case, I might ask you to demo your code, 
and you need to be able to explain what your code is doing and you'll need 
to justify why you chose to do things the way you did.
"""
import sys # for sys.argv
import numpy as np # 1d, 2d and 3d arrays
import math # logarithmic
import random # random number range
import itertools # read line ranges

# mapping the observation symbols to index
obsSymbols = ' abcdefghijklmnopqrstuvwxyz'
obsSymbols2Index = {}
for index,character in enumerate(obsSymbols):
    obsSymbols2Index[character] = index
# set maximum T in case read in extra lines from line
maxT = 50000

"""
2. Initialize 
    (b) three matrices A, B, and pi. 
        let pi_i ~ 1/N and let a_ij ~ 1/N and b_j(k) ~ 1/M. 
        verify the row stochastic conditions
"""
class modelLambda:
    def __init__(self, N, M):
        """                        
        # A = {a_ij} is NxN 
             # q0    q1
        A = [[1.0/N, 1.0/N], # q0 
             [1.0/N, 1.0/N]] # q1
        """
        a_init = 1.0/N                                               
        a_delta = a_init/100.
        a = np.random.uniform(a_init-a_delta, a_init+a_delta, [N,N])
        a /= a.sum(axis=1)[:,None] # row stochatic
        print "init A = "
        for row in range(len(a[:,0])):
            print a[row,:], sum(a[row,:])
        self.A = a

        """                                  
        # B = {b_j(k)} is NxM 
             # ' ' a b c .... z
        B = [[1.0/M for i in range(M)], # q0
             [1.0/M for i in range(M)]] # q1
        """
        b_init = 1.0/M
        b_delta = b_init/100.
        b = np.random.uniform(b_init-b_delta, b_init+b_delta, [N,M])
        b /= b.sum(axis=1)[:,None] # row stochatic
        print "init B = "
        for row in range(len(b[:,0])):
            print b[row,:], sum(b[row,:])
        self.B = b

        """                   
        # pi = {pi_i} is 1xN.
             # q0    q1 
        """
        p_init = 1.0/N                                               
        p_delta = p_init/100.
        p = np.random.uniform(p_init-p_delta, p_init+p_delta, [1,N])
        p /= p.sum(axis=1)[:,None] # row stochatic
        print "init pi = "
        for row in range(len(p[:,0])):
            print p[row,:], sum(p[row,:])
        p = np.array(p).flatten()
        self.pi = p
    
class parameterSet:
    def __init__(self, obs_seq, N):
        # T : length of observation sequence 
        # M : number of observation symbol
        # N : number of states in the model
        T = len(obs_seq)
        self.obsO =  obs_seq # 1d array
        self.scaleC = np.zeros(T) # 1d array
        self.alpha = np.zeros((N, T)) # 2d array
        self.beta = np.zeros((N, T)) # 2d array
        self.gamma = np.zeros((N, T)) # 2d array
        self.digamma = np.zeros((N, N, T))  # 3d array

def selPartOfText(fileName, startCol, startLine, endLine):
    lines = []
    # sel by row range
    print "Sel Text from Line" + str(startLine) + " to Line" + str(endLine)
    with open(fileName, 'r') as f:
        for line in itertools.islice(f, startLine, endLine):
            lines.append(line[startCol:])
    f.close()
    return lines

"""
1. Given
Observation sequence O = (O_0, O_1, . . . , O_{T-1}).
"""
def getObsSeq(fileName, startCol, startLine, endLine): 
    print "get observations sequence...", 
    # read file and select part of text
    lines = selPartOfText(fileName, startCol, startLine, endLine)

    # convert to lower-case and keep only 26 characters and a word space
    obs_seq = []
    for line in lines:
        # remove leading spaces, trimming newline and converting lowercase
        line = (line.lstrip()).rstrip().lower()
        for c in line:
            if c.isspace() or c.isalpha():
                obs_seq.append(obsSymbols2Index[c]) # convert char to number
    # set maximum T in case read in extra lines from line
    if len(obs_seq) > maxT:
        obs_seq = obs_seq[:maxT]
    return obs_seq



"""
3. Forward algorithm or alpha-pass
"""
def alphaPass(lambdaModel, parmSet, N):
    print "alphaPass...",
    A = lambdaModel.A 
    B = lambdaModel.B 
    pi = lambdaModel.pi

    O = parmSet.obsO  # 1d array
    T = len(O) # Length of observation sequence
    c = parmSet.scaleC # 1d array
    alpha = parmSet.alpha # 2d array (list of list)
    """
    // compute alpha_0(i) 
    c_0 = 0
    for i = 0 to N-1
        alpha_0(i) = pi_i * b_i(O_0)
        c_0 = c_0 + alpha_0(i) 
    next i
    """
    c0 = 0
    for i in xrange(N):
        alpha[i][0] = pi[i] * B[i][O[0]]
        c0 += alpha[i][0]
    """
    // scale the alpha_0(i) 
    c_0 = 1/c_0
    for i = 0 to N-1
        alpha_0(i) = c_0 * alpha_0(i) 
    next i
    """
    c[0] = 1/c0
    for i in xrange(N):
        alpha[i][0] *= c[0]
    
    """
    // compute alpha_t(i) 
    for t = 1 to T-1
        c_t = 0
        for i = 0 to N-1
            alpha_t(i) = 0
            for j = 0 to N-1
                alpha_t(i) = alpha_t(i) + alpha_{t-1}(j) * a_ji 
            next j
            alpha_t(i) = alpha_t(i) * b_i(O_t)
            c_t = c_t + alpha_t(i) 
        next i
        // scale alpha_t(i)
        c_t = 1/c_t
        for i = 0 to N-1
            alpha_t(i) = c_t * alpha_t(i) 
        next i
    next t
    """
    for t in xrange(1, T):
        ct = 0
        for i in xrange(N):
            alpha[i][t] = 0
            for j in xrange(N):
                alpha[i][t] += alpha[j][t-1] * A[j][i]
            alpha[i][t] *= B[i][O[t]]
            ct += alpha[i][t]
        c[t] = 1/ct
        for i in xrange(N):
            alpha[i][t] *= c[t]
    return parmSet

"""
4. Backward algorithm or beta-pass
"""
def betaPass(lambdaModel, parmSet, N):
    print "betaPass...",
    A = lambdaModel.A 
    B = lambdaModel.B 
    pi = lambdaModel.pi

    O = parmSet.obsO  # 1d array
    T = len(O) # Length of observation sequence
    c = parmSet.scaleC # 1d array
    beta = parmSet.beta # 2d array (list of list)
    #print "betaPass: " + str(c)
    """
    // Let beta_{T-1}(i) = 1 scaled by c_{T-1}
    for i = 0 to N-1
        beta_{T-1}(i) = c_{T-1}
    next i
    """
    for i in xrange(N):
        beta[i][T-1] = c[T-1] 

    """
    // beta-pass
    for t = T-2 to 0 by -1
        for i = 0 to N-1 
            beta_t(i) = 0
            for j = 0 to N-1
                beta_t(i) = beta_t(i) + a_ij * b_j(O_{t+1}) * beta_{t+1}(j)
            next j
            // scale betat(i) with same scale factor as alpha_t(i) 
            beta_t(i) = c_t * beta_t(i)
        next i 
    next t
    """
    for t in xrange(T-2, -1, -1): #T-1, T-2, ..., 1, 0
        for i in xrange(N):
            beta[i][t] = 0
            for j in xrange(N):
                beta[i][t] += A[i][j] * B[j][O[t+1]] * beta[j][t+1]
            beta[i][t] *= c[t]
    return parmSet

"""
5. Compute the gammas and di-gammas
"""
def gammasPass(lambdaModel, parmSet, N):
    print "gammasPass...",
    A = lambdaModel.A 
    B = lambdaModel.B 
    pi = lambdaModel.pi

    O = parmSet.obsO  # 1d array (1xT)
    T = len(O) # Length of observation sequence
    c = parmSet.scaleC # 1d array (1xT)
    alpha = parmSet.alpha # 2d array (NxT)
    beta = parmSet.beta # 2d array (NxT)
    gamma = parmSet.gamma # 2d array (NxT) 
    digamma = parmSet.digamma # 3d array (NxNxT)
    #print "gammaPass: " + str(c)
    """
    for t = 0 to T-2 
        denom = 0
        for i = 0 to N-1
            for j = 0 to N-1
                denom = denom + alpha_t(i) * a_ij * b_j(O_{t+1}) * beta_{t+1}(j) 
            next j
        next i 
        for i = 0 to N-1
            gamma_t(i) = 0
            for j = 0 to N-1
                gamma_t(i,j) =  (alpha_t(i) * a_ij * b_j(O_{t+1}) * beta_{t+1}(j))/denom
                gamma_t(i) = gamma_t(i) + gamma_t(i,j) 
            next j
        next i 
    next t
    """
    # v1
    for t in xrange(T-1): # t = 0,1,...{(T-1)-1}={T-2}
        denom = 0
        for i in xrange(N):
            for j in xrange(N):
                denom += alpha[i][t] * A[i][j] * B[j][O[t+1]] * beta[j][t+1]
        for i in xrange(N):
            gamma[i][t] = 0
            for j in xrange(N):
                digamma[i][j][t] = (alpha[i][t] * A[i][j] * B[j][O[t+1]] * beta[j][t+1])/denom
                gamma[i][t] += digamma[i][j][t]
                
    """
    // Special case for gamma_{T-1}(i) 
    denom = 0
    for i = 0 to N-1
        denom = denom + alpha_{T-1}(i) 
    next i
    for i = 0 to N-1
        gamma_{T-1}(i) = alpha_{T-1}(i)/denom
    next i
    """
    denom = 0
    for i in xrange(N):
        gamma[i][T-1] = alpha[i][T-1]

    return parmSet

"""
6. Re-estimate the model lambda = (A, B, pi)
"""
def reEstimateModel(lambdaModel, parmSet, N):
    A = lambdaModel.A 
    B = lambdaModel.B 
    pi = lambdaModel.pi

    O = parmSet.obsO  # 1d array (1xT)
    M = len(set(parmSet.obsO)) # Number of observation symbol 
    T = len(O) # Length of observation sequence
    c = parmSet.scaleC # 1d array (1xT)
    alpha = parmSet.alpha # 2d array (NxT)
    beta = parmSet.beta # 2d array (NxT)
    gamma = parmSet.gamma # 2d array (NxT) 
    digamma = parmSet.digamma # 3d array (NxNxT)
    """
    // re-estimate pi
    for i = 0 to N-1
        pi_i = gamma_0(i)
    next i
    """
    print "re-estimate pi...",
    for i in xrange(N):
        pi[i] = gamma[i][0]

    """
    // re-estimate A and B
    for i = 0 to N-1
        denom = 0
        for t = 0 to T-2
            denom = denom + gamma_t(i)
        next t
        for j = 0 to N-1 // re-estimate A
            numer = 0
            for t = 0 to T-2
                numer = numer + gamma_t(i,j)
            next t
            aij = numer/denom 
        next j
        for j = 0 to M-1 // re-estimate B
            numer = 0
            for t = 0 to T-2
                if(O_t == j) then
                    numer = numer + gamma_t(i)
            next t
            bi(j) = numer/denom 
        next j
    next i
    """
    #v1
    print "A...",
    for i in xrange(N):
        for j in xrange(N): 
            numer = denom = 0.0
            for t in xrange(T-1): # t=0,1,...,(T-1)-1=T-2
                numer += digamma[i][j][t]
                denom += gamma[i][t]
            A[i][j] = numer/denom
    print "B...",
    for i in xrange(N):
        for j in xrange(M): 
            numer = denom = 0.0
            for t in xrange(T): # t=0,1,...,T-1
                if (O[t] == j):
                    numer += gamma[i][t]
                denom += gamma[i][t]
            B[i][j] = numer/denom
    """
    #v2
    denom = numer = 0.0
    for i in xrange(N):
        for j in xrange(N): # re-estimate A
            denom = numer = 0.0
            for t in xrange(T-1): # t=0,1,...,(T-1)-1=T-2
                numer += digamma[i][j][t]
                denom += gamma[i][t]
            A[i][j] = numer/denom
    denom = numer = 0.0
    for i in xrange(N):
        for j in xrange(M): # re-estimate B
            denom = numer = 0.0
            for t in xrange(T): # t=0,1,...,(T-1)
                if (O[t] == j):
                    numer += gamma[i][t]
                denom += gamma[i][t]
            B[i][j] = numer/denom
    """
    return lambdaModel


def main():
    if len(sys.argv) != 5:
        print """
        USAGE:
        hmmQ4.py textFile  startCol  startLine  endLine 
                 [1]       [2]       [3]        [4]     
        1. textFile:   file name to be read/analyzed (only 1 file)
        2. startCol:   starting column number to be read in
        3. startLine:  starting line number to be read in 
        4. endLine:    ending line number to stop read in 
        """
        exit(1)
    else:
        fileName = sys.argv[1]
        startCol = int(sys.argv[2])
        startLine = int(sys.argv[3])
        endLine = int(sys.argv[4])

        ######################################################
        N = 2  # Number of states in the model
        M = 27 # Number of observation symbols (a,b,...z,' ')
        ######################################################
        print "Input: N = " + str(N) + " M = " + str(M)
        """
        1. Given
        Observation sequence O = (O_0, O_1, . . . , O_{T-1}).
        """
        ObsSeqO = getObsSeq(fileName, startCol, startLine, endLine)
        parmSet_init = parameterSet(ObsSeqO, N)
        T = len(parmSet_init.obsO) # Length of Observation Sequence
        print "T = " + str(T)
        #print parmSet_init.obsO

        """
        2. Initialize 
            (a) Select N and determine M from O. 
        """
        M = len(set(parmSet_init.obsO)) # Number of observation symbol 
        print "M = " + str(M)
        
        """
        2. Initialize 
            (b) three matrices A, B, and pi. 
                let pi_i ~ 1/N and let a_ij ~ 1/N and b_j(k) ~ 1/M. 
                verify the row stochastic conditions
        """
        lambdaM_init = modelLambda(N, M)

        """
        2. Initialize 
            (c) maxIters = maximum number of re-estimation iterations
                iters = 0
                oldLogProb = -inf
        """
        maxIters = 100
        iters = 0
        oldLogProb = float("-inf")
        logProb = 0.0

        if not False:
            while True:                                                     
            ########################## Start of while-iteration
                if iters == 0:
                    lambdaM = lambdaM_init
                    parmSet = parmSet_init
                
                """                                                      
                3. Forward algorithm or alpha-pass
                """
                alphaPass(lambdaM, parmSet, N)
                #print parmSet.alpha
                                                                        
                """
                4. Backward algorithm or beta-pass
                """
                betaPass(lambdaM, parmSet, N)
                #print parmSet.beta
                                                                        
                """
                5. Compute the gammas and di-gammas
                """
                parmSet_new = gammasPass(lambdaM, parmSet, N)
                #print parmSet.digamma; print np.shape(parmSet.digamma)
                #print parmSet_new.gamma; print np.shape(parmSet_new.gamma)
                                                                        
                """
                6. Re-estimate the model lambda = (A, B, pi)
                """
                lambdaM_new = reEstimateModel(lambdaM, parmSet_new, N)
                #print lambdaM_new.B, sum(lambdaM_new.B[0])
                #print lambdaM_new.A, sum(lambdaM_new.A[0])
                                                                            
                """
                7. Compute log P(O|lambda)
                logProb = 0
                for i = 0 to T-1
                    logProb = logProb + log(c_i) 
                next i
                logProb = -logProb
                """
                c = parmSet_new.scaleC # 1d array (1xT)         
                O = parmSet_new.obsO  # 1d array (1xT)
                logProb = 0.0
                for t in xrange(T):
                    logProb += math.log(c[t])
                logProb *= -1
                                                                            
                """                                                    
                8. To iterate or not to iterate, that is the question.
                    iters = iters + 1
                    if(iters < maxIters or logProb > oldLogProb) then
                        oldLogProb = logProb
                        goto alpha-pass 
                    else
                        output lambda = (A, B, pi) 
                    end if
                """
                iters += 1
                if (iters < maxIters) and (logProb > oldLogProb):
                    print
                    print "iter=%d, logProb=%8.2f, oldLogProb=%8.2f" %\
                            (iters, logProb, oldLogProb)
                    oldLogProb = logProb
                    lambdaM = lambdaM_new
                    parmSet = parmSet_new
                    continue
                else:
                    finalB = np.transpose(lambdaM_new.B)
                    print
                    print 'Final B = '
                    for idx,symbol in enumerate(obsSymbols):
                        print symbol, finalB[idx]
                    print sum(lambdaM_new.B[0]), sum(lambdaM_new.B[1])
                    print 'Final pi = '
                    print lambdaM_new.pi
                    print 'Final A = '
                    print lambdaM_new.A
                    exit(0)
                
            ##########################
            # End of while-iteration

if __name__ == '__main__':
    main()

