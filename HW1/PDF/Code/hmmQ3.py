"""
CS271 - Fall2019 - HW1 - Inhee Park (7405)

Problem #3

a) By a similar direct calculation, 
compute P(O|lambda) for each observation sequence of the form 
O = (O0, O1, O2, O3), where Oi={0, 1, 2}. 
Verify that P(O|lambda) = 1, where the sum is over the observation 
sequences of length four. Note that you will need to use the 
probabilities for A, B, and pi given in equations (2.4), (2.5), 
and (2.6) in Section 2.2, respectively.

b) Use the forward algorithm to compute P(O|lambda) for the same 
observation sequences and model as in part a). 
Verify that you obtain the same results as in part a).

"""
import sys # for sys.argv
import numpy as np # 1d, 2d and 3d arrays
import math # logarithmic
import random # random number range
import itertools # read line ranges

idx2Q = {0:'H', 1:'C'} # state mapping to index

class modelLambda:
    # N = 2 (states in model, V = {H, C})
    # M = 3 (observation symbols, Q = {0, 1, 2})
    # T = 4 (length of observation sequence)
    def __init__(self, N, M):
        # A = {a_ij} is NxN 
                  # H   C
        self.A = [[0.7, 0.3], # H
                  [0.4, 0.6]] # C

        # B = {b_j(k)} is NxM 
            # 0    1    2 
        self.B = [[0.1, 0.4, 0.5],  # H
                  [0.7, 0.2, 0.1]] # C

        # pi = {pi_i} is 1xN.
        self.pi = [0.6, 0.4]
    
class parameterSet:
    def __init__(self, obs_seq, N, T):
        # T : length of observation sequence 
        # M : number of observation symbol
        # N : number of states in the model
        self.obsO =  obs_seq # 1d array
        self.scaleC = np.zeros(T) # 1d array
        self.alpha = np.zeros((N, T)) # 2d array

# Forward algorithm or alpha-pass
def alphaPass(lambdaModel, parmSet, N, M, T):
    A = lambdaModel.A 
    B = lambdaModel.B 
    pi = lambdaModel.pi
    #print B

    O = parmSet.obsO  # 1d array
    c = parmSet.scaleC # 1d array
    alpha = parmSet.alpha # 2d array (list of list)
    #print "alphaPass: " + str(c)
    """
    // compute alpha_0(i) 
    c_0 = 0
    for i = 0 to N-1
        alpha_0(i) = pi_i * b_i(O_0)
        c_0 = c_0 + alpha_0(i) 
    next i
    """
    c0 = 0.0
    for i in xrange(N):
        alpha[i][0] = pi[i] * B[i][O[0]]
        c0 += alpha[i][0]
    """
    // scale the alpha_0(i) 
    c_0 = 1/c_0
    for i = 0 to N-1
        alpha_0(i) = c_0 * alpha_0(i) 
    next i
    """
    c[0] = 1.0/c0
    for i in xrange(N):
        alpha[i][0] *= c[0]
    
    """
    // compute alpha_t(i) 
    for t = 1 to T-1
        c_t = 0
        for i = 0 to N-1
            alpha_t(i) = 0
            for j = 0 to N-1
                alpha_t(i) = alpha_t(i) + alpha_{t-1}(j) * a_ji 
            next j
            alpha_t(i) = alpha_t(i) * b_i(O_t)
            c_t = c_t + alpha_t(i) 
        next i
        // scale alpha_t(i)
        c_t = 1/c_t
        for i = 0 to N-1
            alpha_t(i) = c_t * alpha_t(i) 
        next i
    next t
    """
    for t in xrange(1, T):
        ct = 0.0
        for i in xrange(N):
            alpha[i][t] = 0.0
            for j in xrange(N):
                alpha[i][t] += alpha[j][t-1] * A[j][i]
            alpha[i][t] *= B[i][O[t]]
            ct += alpha[i][t]
        c[t] = 1.0/ct
        for i in xrange(N):
            alpha[i][t] *= c[t]
    return parmSet

def obsSeqEnumeration(M, T):
    obsSeqList = []
    for obsSeq in itertools.product(range(M), repeat=T):
        obsSeqList.append(obsSeq)
    return obsSeqList 

def directCalcProb(obsSeq, N, T, lambdaM, printCtrl=0):
    A = lambdaM.A
    B = lambdaM.B
    pi = lambdaM.pi
    #
    # Per input obsSeq e.g. O = (0,1,0,2)
    # There are total 2^4=16 hidden states X's,
    # where X={HHHH,HHHC,HHCH,....,CCCC), where Xi=0,1,..,15
    # Prob(O=(0,1,0,2),Xi=x0x1x2x3) =
    # = pi_x0 b_x0(O0) a_x0x1 b_x1(O1) a_x1x2 b_x2(O2) a_x2x3 b_x3(O3)
    # 
    prob_OiXall = {}
    for X in itertools.product(range(N), repeat=T):
        seqX = ''.join([idx2Q[i] for i in X]) 
        prob_OiXi = pi[X[0]] * B[X[0]][obsSeq[0]] * A[X[0]][X[1]] *\
                B[X[1]][obsSeq[1]] * A[X[1]][X[2]] * B[X[2]][obsSeq[2]] *\
                A[X[2]][X[3]] * B[X[3]][obsSeq[3]]
        if printCtrl > 0:
            print "::P(O=" + str(obsSeq).replace(' ', '') + ", X=" + seqX +\
                    ") = " + str(prob_OiXi)
        prob_OiXall[seqX] = prob_OiXi
    return prob_OiXall

def forwardAlgorithmCalcProb(obsSeq, N, T, lambdaM, parmSet, printCtrl=0):
    A = lambdaM.A
    B = lambdaM.B
    pi = lambdaM.pi
    #
    # Per input obsSeq e.g. O = (0,1,0,2)
    # There are total 16 hidden states X's,
    # where X={HHHH,HHHC,HHCH,....,CCCC), where Xi=0,1,..,15
    # Prob(O=(0,1,0,2),Xi=x0x1x2x3) =
    # = alpha_0(0) + alpha_0(1) + alpha_1(0) + alpha_1(1) + 
    #   alpha_2(0) + alpha_2(1) + alpha_3(0) + alpha_3(1)
    # 
    prob_OiXall = {}
    for X in itertools.product(range(N), repeat=T):
        seqX = ''.join([idx2Q[i] for i in X]) 
        prob_OiXi = pi[X[0]] * B[X[0]][obsSeq[0]] * A[X[0]][X[1]] *\
                B[X[1]][obsSeq[1]] * A[X[1]][X[2]] * B[X[2]][obsSeq[2]] *\
                A[X[2]][X[3]] * B[X[3]][obsSeq[3]]
        if printCtrl > 0:
            print "::P(O=" + str(obsSeq).replace(' ', '') + ", X=" + seqX +\
                    ") = " + str(prob_OiXi)
        prob_OiXall[seqX] = prob_OiXi
    return prob_OiXall

def main():
    ######################################################
    N = 2 # Number of states in the model
    M = 3 # Number of observation symbols (0, 1, 2)
    T = 4 # Length of observation sequence
    ######################################################
    print "N = " + str(N) + " M = " + str(M) + " T = " + str(T)
    lambdaM = modelLambda(N, M)

    # generate all possible 3^4 = 81 observation sequences
    obsSeqList = obsSeqEnumeration(M, T)
    
    # direct calculation
    #obsSeq = [0, 1, 0, 2] # P(O) = 0.009629
    print_obsSeq = [(0,1,0,2)]

    allPs = 0.0
    for count, obsSeq in enumerate(obsSeqList):
        #print "ObsSeq# " + str(count+1) + " : " + str(obsSeq)
        thisPs = 0.0
        if obsSeq in print_obsSeq:
            print "------------------------------------"
            p_info = directCalcProb(obsSeq, N, T, lambdaM, 1)
        else:
            p_info = directCalcProb(obsSeq, N, T, lambdaM, 0)
        for k in p_info.keys():
           thisPs += p_info[k] 
        print "P(O=%s|lambda) = %7.5f" % (str(obsSeq).replace(' ', ''), thisPs)
        if obsSeq in print_obsSeq:
            print "------------------------------------"
        allPs += thisPs
    print "================================="
    print "Sum{P(O|lambda)} = " + str(allPs)
    print "by direct calculation "
    print "================================="

    # forward algorithm
    allPs = 0.0
    for count, obsSeq in enumerate(obsSeqList):
        parmSet = parameterSet(obsSeq, N, T)
        #print "ObsSeq# " + str(count+1) + " : " + str(obsSeq)
        thisPs = 0.0
        if obsSeq in print_obsSeq:
            print "------------------------------------"
            p_info = forwardAlgorithmCalcProb(obsSeq, N, T, lambdaM, parmSet, 1)
        else:
            p_info = forwardAlgorithmCalcProb(obsSeq, N, T, lambdaM, parmSet, 0)
        for k in p_info.keys():
           thisPs += p_info[k] 
        print "P(O=%s|lambda) = %7.5f" % (str(obsSeq).replace(' ', ''), thisPs)
        if obsSeq in print_obsSeq:
            print "------------------------------------"
        allPs += thisPs
    print "================================="
    print "Sum{P(O|lambda)} = " + str(allPs) 
    print "by forward algorithm"
    print "================================="


        

if __name__ == '__main__':
    main()
