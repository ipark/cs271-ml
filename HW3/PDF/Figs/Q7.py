import matplotlib.pyplot as plt

X, Y = [], []
for line in open('Q7.data', 'r'):
    if "x" not in line:
       values = [float(s) for s in line.split()]   
       X.append(values[0])
       Y.append(values[1])

ax = plt.gca()
ax.tick_params(axis = 'both', which = 'major', labelsize = 20)
ax.axvspan(0, 0.1, ymax=0.2, alpha=0.4, color='blue')
plt.xlim((0,1))
plt.ylim((0,1))
plt.ylabel('TPR', fontsize = 20)
plt.xlabel('FPR', fontsize = 20)
plt.title('ROC Curve', fontsize = 20)
plt.plot(X, Y)
plt.grid()
plt.scatter(X, Y, s=80)
plt.savefig('Q7-roc.png')
plt.show()
