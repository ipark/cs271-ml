import matplotlib.pyplot as plt

X, Y = [], []
for line in open('Q9.data', 'r'):
    if "x" not in line:
       values = [float(s) for s in line.split()]   
       X.append(values[0])
       Y.append(values[1])

ax = plt.gca()
ax.tick_params(axis = 'both', which = 'major', labelsize = 20)
ax.fill_between(X, Y, alpha=0.4)
plt.xlim((0,1))
plt.ylim((0,1))
plt.xlabel('Recall=TPR', fontsize = 20)
plt.ylabel('Precision=TP/(TP+FP)', fontsize = 20)
plt.title('PR Curve w/ x10 Nomatch Score', fontsize = 20)
plt.plot(X, Y)
plt.grid()
plt.scatter(X, Y, s=80)
plt.savefig('Q9-pr.png')
plt.show()
