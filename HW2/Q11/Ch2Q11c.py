import random
import sys
import itertools
import numpy as np

# mapping the observation symbols to index
obsSymbols = 'abcdefghijklmnopqrstuvwxyz'
obsSymbols2Index = {}
obsIndex2Symbols = {}
for index,character in enumerate(obsSymbols):
    obsSymbols2Index[character] = index
    obsIndex2Symbols[index] = character
# set maximum T in case read in extra lines from line

def ShiftEncrypt(plainTextIdx):
    plainText = ''.join([obsIndex2Symbols[i] for i in plainTextIdx])
    key = random.randrange(len(obsSymbols2Index))
    cipherTextIdx = [(i + key) % len(obsIndex2Symbols) for i in plainTextIdx]
    cipherText = ''.join([obsIndex2Symbols[i] for i in cipherTextIdx])
    T = len(cipherTextIdx)
    M = len(set(cipherTextIdx))
    print "In py: T=%d" % (T)
    print "In py: M=%d" % (M)
    O = ' '.join([str(x) for x in cipherTextIdx])
    with open("T.out", "w") as f:
        f.write("%d %d" % (T, M))
    with open("O.out", "w") as f:
        f.write(O)
    with open("key.out", "w") as f:
        f.write(str(key))

def selPartOfText(fileName, startCol, startLine, endLine):
    lines = []
    # sel by row range
    print "Sel Text from Line" + str(startLine) + " to Line" + str(endLine)
    with open(fileName, 'r') as f:
        for line in itertools.islice(f, startLine, endLine):
            lines.append(line[startCol:])
    f.close()
    return lines

def getObsSeq(fileName, startCol, startLine, endLine, maxT): 
    print "get observations sequence...", 
    # read file and select part of text
    lines = selPartOfText(fileName, startCol, startLine, endLine)

    # convert to lower-case and keep only 26 characters and a word space
    obs_seq = []
    for line in lines:
        # remove leading spaces, trimming newline and converting lowercase
        line = (line.lstrip()).rstrip().lower()
        for c in line:
            if c.isalpha(): #if c.isspace() or c.isalpha():
                obs_seq.append(obsSymbols2Index[c]) # convert char to number
    # set maximum T in case read in extra lines from line
    if len(obs_seq) > maxT:
        obs_seq = obs_seq[:maxT]
    return obs_seq

def digraphA(plainTextIdx):
   
    T = len(plainTextIdx)
    M = len(set(plainTextIdx))
    A = np.zeros((M, M), dtype = float)
    a = np.full((M, M), 5, dtype = int) # add 5
    I = np.array(plainTextIdx) # original sequence
    J = np.roll(I, -1) # shift left by 1 sequence
    #print I[:10]
    #print J[:10]
    for i in range(T):
        #print "a_" + str(I[i]) + ","+ str(J[i]),
        A[I[i]][J[i]] += 1
    A = np.add(A, a)
    for i in xrange(M):
        A[i, :] /= np.sum(A[i, :])
    
    for i in xrange(M):
        print np.sum(A[i, :])

    np.set_printoptions(precision=5)
    print A
    A = ' '.join(format(x, ".5f") for x in A.flatten())
    with open("A.out", "w") as f:
        f.write(str(A))

def main():
    # make sure "old" files are removed for c not to read "old" ones
    import os
    if os.path.isfile("A.out"):
        os.remove("A.out")

    if len(sys.argv) != 6:
        print """
        USAGE:
        Ch2Q11c.py textFile  startCol  startLine  endLine maxT 
                    [1]       [2]       [3]        [4]     [5] 
        1. textFile:   file name to be read/analyzed (only 1 file)
        2. startCol:   starting column number to be read in
        3. startLine:  starting line number to be read in 
        4. endLine:    ending line number to stop read in 
        5. maxT:       maximum length of observation sequence 
        """
        exit(1)
    else:
        fileName = sys.argv[1]       
        startCol = int(sys.argv[2])
        startLine = int(sys.argv[3])
        endLine = int(sys.argv[4])
        maxT = int(sys.argv[5])

        """
        1. Given
        Observation sequence O = (O_0, O_1, . . . , O_{T-1}).
        """
        plainTextIdx = getObsSeq(fileName, startCol, startLine, endLine, maxT)
        digraphA(plainTextIdx)
        #ShiftEncrypt(plainText)

if __name__ == '__main__':
    main()

