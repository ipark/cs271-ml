import sys
obsSymbols = 'abcdefghijklmnopqrstuvwxyz'
obsSymbols2Index = {}
obsIndex2Symbols = {}
for index,character in enumerate(obsSymbols):
    obsSymbols2Index[character] = index
    obsIndex2Symbols[index] = character

def matchScore(hmmOutput, M, key):
    with open(hmmOutput, 'r') as f:
        lines = f.readlines()
    f.close()
    lines = lines[1:-2]
    c=zip(*[line.split() for line in lines])[0]
    putativeKeys = []
    for i in range(1, M):
        x=zip(*[line.split() for line in lines])[i]
        maxIdx = x.index(max(x))
        putativeKeys.append(c[maxIdx])
        #print str(maxIdx % M) + "=" +  c[maxIdx] + "; Prob=" + str(max(x))
    #print putativeKeys
    realKeys = [obsIndex2Symbols[(i + key) % M] for i,c in enumerate(obsSymbols)]
    #print realKeys
    mismatch = [c for i, c in enumerate(putativeKeys) if c is not realKeys[i]]
    print mismatch
    print "score:%.4f" % (1.0*(M-len(mismatch))/M)

def matchScore2(hmmOutput, M, key):
    with open(hmmOutput, 'r') as f:
        lines = f.readlines()
    f.close()
    lines = lines[1:-2]
    c=zip(*[line.split() for line in lines])[0]
    mismatch = 0
    for i in range(1, M):
        x=zip(*[line.split() for line in lines])[i]
        maxIdx = x.index(max(x))
        print maxIdx, (i+key)%M
        if maxIdx != (i + key - 1) % M:
            mismatch += 1
    print mismatch
    print "score:%.4f" % (1.0*(M-mismatch)/M)

def main():
    if len(sys.argv) != 4:
        print """
        USAGE:
        matchScore.py  hmmOutputFile   M     key
                       [1]             [2]   [3]
        1. hmmOutputFile:   transpose B-matrix
        2. M:               number of observation symbols
        3. key:             key used for shift encryption
        """
        exit(1)
    else:
        hmmOutput = sys.argv[1]       
        M = int(sys.argv[2])
        key = int(sys.argv[3])
        matchScore2(hmmOutput, M, key)

if __name__ == '__main__':
    main()

