#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

/**
 * CS271 - Fall2019 - Inhee Park
 * gcc -o hmmCh2Q11d -Wall -O3 hmmCh2Q11d.c
 */
int N; // user input
int T; // will be determined from ObsSeq O[]
int M; // will be determined from len(set(O[]])
int K = 5; // random fluctuation (1/N)/K
int maxT; // limit to be used for hmm from ciphertext
int key;

float **A; // float A[N][N];
float **B; // float B[N][M];
float *pi; // float pi[N];
int *O; // int O[T];
float *c; // float c[T];
float **alpha; // float alpha[T][N]
float **beta; // float beta[T][N]
float **Gamma; // float Gamma[T][N]
float ***diGamma; // float diGamma[T][N][N]

/**
  set probabilities, p[i] for i = 0, 1, ..., n-1 such that
    0) p[i] ~ p0 +/- f, 
       where p0 = 1/M or 1/N, 
             f = p0/K (K = 5),
             +/- can be done by r = 2*f*r - f; // [0,2f]->[-f,f]
    1) 0 <= p[i] < 1
    2) sum of p[i] = 1
 */

void set_probabilities(float *p, int n, float p0, float f)
{
    float sum = 0.f;
//  printf("p0 = %f; f = %f\n", p0, f);
    for (int i = 0; i < n; ++i) {
        float r = rand() / (float) RAND_MAX; // [0, RAND_MAX] -> [0, 1]
//      printf("r = %f\n", r);
        r = 2 * f * r - f; // [0, 2f] -> [-f, f]
//      printf("r = %f; f = %f\n", r, f);
//      printf("p0 + r = %.3f + (%.3f)\n", p0, r);
        p[i] = p0 + r;
        sum += p0 + r;
    }
    // normalize
    for (int i = 0; i < n; ++i) {
        p[i] /= sum;
    }
}

#include <unistd.h>
time_t seed;
void initLambdaModel()
{
    seed = time(NULL) * getpid();
    srand(seed);
    float p0 = (float) 1 / N;

    // init pi
    set_probabilities(pi, N, p0, p0 / K);

    // init A => 
    // no approximate initialization
    // for this problem Ch2-Q11d
    // instead use the pre-computed digraph from
    // Ch2-Q11c
    /*
    for (int i = 0; i < N; ++i) {
        set_probabilities(A[i], N, p0, p0 / K);
    }
    */

    //printf("M=%d\n", M);
    float q0 = (float) 1 / M;
    for (int i = 0; i < N; ++i) {
        set_probabilities(B[i], M, q0, q0 / K);
    }
}


void alphaPass(void) {
    c[0] = 0;
    for (int i = 0; i < N; ++i) {
        alpha[0][i] = pi[i] * B[i][O[0]];
        c[0] += alpha[0][i];
    }

    c[0] = 1 / c[0];
    for (int i = 0; i < N; ++i) {
        alpha[0][i] *= c[0];
    }
    for (int t = 1; t < T; ++t) {
        c[t] = 0;
        for (int i = 0; i < N; ++i) {
            alpha[t][i] = 0;
            for (int j = 0; j < N; ++j) {
                alpha[t][i] += alpha[t - 1][j] * A[j][i];
            }
            alpha[t][i] *= B[i][O[t]];
            c[t] += alpha[t][i];
        }
        c[t] = 1 / c[t];
        for (int i = 0; i < N; ++i) {
            alpha[t][i] *= c[t];
        }
    }
}

void betaPass(void) {
    for (int i = 0; i < N; ++i) {
        beta[T - 1][i] = c[T - 1];
    }

    for (int t = T - 2; t >= 0; --t) {
        for (int i = 0; i < N; ++i) {
            beta[t][i] = 0;
            for (int j = 0; j < N; ++j) {
                beta[t][i] += A[i][j] * B[j][O[t + 1]] * beta[t + 1][j];
            }
            beta[t][i] *= c[t];
        }
    }
}

void GammasPass(void) {
    for (int t = 0; t < T - 1; ++t) {
        float denom = 0;
        for (int i = 0; i < N; ++i) {
            for (int j = 0; j < N; ++j) {
                denom += alpha[t][i] * A[i][j] * B[j][O[t + 1]] * beta[t + 1][j];
            }
        }
        for (int i = 0; i < N; ++i) {
            Gamma[t][i] = 0;
            for (int j = 0; j < N; ++j) {
                diGamma[t][i][j] = alpha[t][i] * A[i][j] * B[j][O[t + 1]] * beta[t + 1][j] / denom;
                Gamma[t][i] += diGamma[t][i][j];
            }
        }
    }
    float denom = 0;
    for (int i = 0; i < N; ++i) {
        denom += alpha[T - 1][i];
    }
    for (int i = 0; i < N; ++i) {
        Gamma[T - 1][i] = alpha[T - 1][i] / denom;
    }
}

void reEstimateModel()
{
    // re-estimate pi
    for (int i = 0; i < N; ++i) {
        pi[i] = Gamma[0][i];
    }

    // re-estimate A and B
    for (int i = 0; i < N; ++i) {
        float denom = 0.f;
        for (int t = 0; t < T - 1; ++t) {
            denom += Gamma[t][i];
        } // EO-for t
        // no re-estimate A for Ch2-Q11d
        // instead use the pre-computed digraph from Ch2-Q11c
        // throughout training
        /*
        for (int j = 0; j < N; ++j) {
            float numer = 0.f;
            for (int t = 0; t < T - 1; ++t) {
                numer += diGamma[t][i][j];
            }
            A[i][j] = numer / denom;
        } // EO-for j
        */
        for (int j = 0; j < M; ++j) {
            float numer = 0.f;
            for (int t = 0; t < T - 1; ++t) {
                if (O[t] == j) {
                    numer += Gamma[t][i];
                }
            } // EO-for t
            B[i][j] = numer / denom;
        } // EO-for j
    } // EO-for i
}

void is_allocated(char var[], void *p)
{
    if (p == NULL) {
        fprintf(stderr, "\"%s\" malloc failed\n", var);
        exit(1);
    }
}

void allocate_memory(void)
{
    //float pi[N];
    pi = (float *) malloc(N * sizeof(float));
    is_allocated("pi", pi);

    //float A[N][N];
    A = (float **) malloc(N * sizeof(float *)); 
    is_allocated("A", A);

    //float B[N][M];
    B = (float **) malloc(N * sizeof(float *));
    is_allocated("B", B);

    for (int i = 0; i < N; ++i) {
        A[i] = (float *) malloc(N * sizeof(float));
        is_allocated("A[i]", A[i]);
        B[i] = (float *) malloc(M * sizeof(float));
        is_allocated("B[i]", B[i]);
    } // EO-for i

    //int O[T];
    O = (int *) malloc(T * sizeof(int));
    is_allocated("O", O);

    //float c[T];
    c = (float *) malloc(T * sizeof(float));
    is_allocated("c", c);

    //float alpha[T][N];
    alpha = (float **) malloc(T * sizeof(float *));
    is_allocated("alpha", alpha);

    //float beta[T][N];
    beta = (float **) malloc(T * sizeof(float *));
    is_allocated("beta", beta);

    //float Gamma[T][N];
    Gamma = (float **) malloc(T * sizeof(float *));
    is_allocated("Gamma", Gamma);

    //float diGamma[T][N][N];
    diGamma = (float ***) malloc(T * sizeof(float **));
    is_allocated("diGamma", diGamma);

    for (int t = 0; t < T; ++t) {                            
        alpha[t] = (float *) malloc(N * sizeof(float));        
        is_allocated("alpha[t]", alpha[t]);

        beta[t] = (float *) malloc(N * sizeof(float));
        is_allocated("beta[t]", beta[t]);

        Gamma[t] = (float *) malloc(N * sizeof(float));
        is_allocated("Gamma[t]", Gamma[t]);

        diGamma[t] = (float **) malloc(N * sizeof(float *));
        is_allocated("diGamma[t]", diGamma[t]);

        for (int i = 0; i < N; ++i) {
            diGamma[t][i] = (float *) malloc(N * sizeof(float));
            is_allocated("diGamma[t][i]", diGamma[t][i]);
        } // EO-for i
    } // EO-for t
} // EO-allocate_memory

void free_memory(void)
{
    free(pi); //float pi[N];
    for (int i = 0; i < N; ++i) {   
        free(A[i]); //float A[N][N];
        free(B[i]); //float B[N][M];
    }
    free(A); //float A[N][N];
    free(B); //float B[N][M];
    free(c); //float c[T];
    free(O); //int O[T];

    for (int t = 0; t < T; ++t) {
      for (int i = 0; i < N; ++i) {
        free(diGamma[t][i]);
      }
      free(diGamma[t]);
      free(Gamma[t]); //float Gamma[T][N]; 
      free(alpha[t]); //float alpha[T][N];
      free(beta[t]); //float beta[T][N];
    }
    free(diGamma); //float diGamma[T][N][N];
    free(Gamma); //float Gamma[T][N];
    free(alpha); //float alpha[T][N];
    free(beta); //float beta[T][N];
}

void restart(int maxIters)
{
    //
    initLambdaModel();

    ///////////////////////////////
    // 2. Initialize iters = 0; oldLogProb = -inf
    ///////////////////////////////
    int iters = 0;
    double logProb = 0.0;
    double oldLogProb = -INFINITY;

    while (iters < maxIters) {
        // 3. Forward algorithm or alpha-pass
        alphaPass();

        // 4. Backward algorithm or beta-pass
        betaPass();

        // 5. Compute the Gammas and di-Gammas
        GammasPass();

        // 6. Re-estimate the B and pi only (no A)
        reEstimateModel();
        //printf("%d: %.4f\n", iters, B[10][10]);

        // 7. Compute log P(O|lambda)
        for (int i = 0; i < T; ++i) {
            logProb += log(c[i]);
        }
        logProb = -logProb;

        logProb = 0.0;
        for (int t = 0; t < T; ++t) {
            logProb += log(c[t]);
        }
        logProb *= -1;

        // 8. To iterate or not to iterate, that is the question.
        iters += 1;
        //printf("iters = %d | %.5f\t(%.5f)\n", iters, logProb, oldLogProb);
        if ((iters < maxIters) || (logProb > oldLogProb)) {
            oldLogProb = logProb;
        } else {
            break;
        }
    } //  EO-while
    printf("--Final transpose(B) matrix-----\n");
    for (int i = 0; i < M; ++i) {
        printf("%c ", i + 97); // print letter
        for (int j = 0; j < N; ++j) {
            printf("%.2f ", B[j][i]);
        }
        printf("\n");
    }
    // row stochastics
    printf("# ");
    for (int i = 0; i < N; ++i) {
        float sum_row = 0.f;
        for (int j = 0; j < M; ++j) {
            sum_row += B[i][j];
        }
        printf("%.2f ", sum_row);
    }
    printf("\n");
    int scores = 0;
    for (int i = 0; i < N; ++i) {
        int max_index = 0;
        float max = B[i][0];
        for (int j = 1; j < M; ++j) {
            if (B[i][j] > max) {
                max_index = j;
                max = B[i][j];
            }
        }
        //printf("i = %d; max_index = %d, max = %f\n", i, max_index, max);
        if (max_index == (i + key) % M) {
            scores += 1;
        }
    }
    printf("match fraction = %.4f\n", 1.0*scores/M);
} // EO-restart

int main(int argc, char *argv[]) {

    //int maxIters;
    //double logProb, oldLogProb;

    if (argc != 7) {
        printf("\n\n");
        printf("USAGE:\n");
        printf("./hmmCh2Q11d    initAmatFile cipherTextIdxFile N    maxIters   maxT  num_restart\n");
        printf("                [1]          [2]               [3]  [4]        [5]   [6]\n");
        printf("eg../hmmCh2Q11d A.out        cipherO.out       26   200        1000  1000\n");
        printf("[1] initAmatFile:       pre-calculated A matrix from digraph from Ch2-Q11(c)\n");
        printf("[2] cipherTextIdxFile:  pre-processed cipherText converted to index values (0-25)\n"); 
        printf("                        from Ch2-Q11(a)\n");
        printf("[3] N:                  number of states   \n");                      
        printf("[4] maxIters:           maximum number of iteration\n");
        printf("[5] maxT:               maximum length of observation sequence\n\n");
        printf("[6] num_restart:        how many restart\n\n");
        exit(1);
    } // EO-if
    N = atoi(argv[3]);
    int maxIters = atoi(argv[4]);
    maxT= atoi(argv[5]);
    int num_restart = atoi(argv[6]);
    //
    // read T.out file spitted out from python execution containing T number
    FILE *f;
    f = fopen("T.out", "r");
    if (fscanf(f, "%d", &T) != 1) {
        fprintf(stderr, "FATAL: read error for T\n");
        exit(1);
    }
    if (fscanf(f, "%d", &M) != 1) {
        fprintf(stderr, "FATAL: read error for M\n");
        exit(1);
    }
    //printf("T: %d\n", T);
    //printf("M: %d\n", M);
    fclose(f);

    // KEY 
    FILE *g;
    g = fopen("key.out", "r");
    if (fscanf(g, "%d", &key) != 1) {
        fprintf(stderr, "FATAL: read error for key\n");
        exit(1);
    }

    // all T, M, N are set, now allocate memories dynamically
    allocate_memory();

    //
    // read cipherO.out file from python execution 
    // convert the read-in number sequences to array O
    f = fopen("cipherO.out", "r");
    // int O[T];
    int Oi = 0, n = 0;
    while(fscanf(f, "%d", &Oi) > 0) {
        if (n == maxT) {
            //printf("maxT=%d; n=%d\n", maxT, n);
            T = n;
            break;
        } else {
            O[n++] = Oi;
        }
    }
    fclose(f);
    printf("N=%d; M=%d; T=%d; maxIters=%d\n", N, M, T, maxIters);


    // 2. Initialize three matrices A, B, and pi.
    // let pi_i ~ 1/N and b_j(k) ~ 1/M, 
    // verify the row stochastic conditions
    // read A.out file from python execution where digraph was generated
    // convert the read-in number to A matrix (later, don't re-estimate)
    f = fopen("A.out", "r");
    float Aij = 0.f;
    for (int i = 0; i < M; ++i) {
        for (int j = 0; j < M; ++j) {
            if (fscanf(f, "%f", &Aij) > 0) {
                A[i][j] = Aij;
            }
        }
    }
    fclose(f);
    //printf("A is initlized\n");
    //


    for (int n = 0; n < num_restart; ++n) {
        printf("restart = %d : ", n);
        restart(maxIters);
    }

    free_memory();

    return 0;
} // EO-main()


