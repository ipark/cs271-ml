import random
import sys
import itertools

"""
Ch2-Q11A
In this problem, you will use an HMM to break a simple substitution
ciphertext message. For each HMM, train using 200 iterations of the
Baum-Welch re-estimation algorithm.
Obtain an English plaintext message of 50,000 plaintext characters,
where the characters consist only of lower case a through z (i.e.,
remove all punctuation, special characters, and spaces, and convert
all upper case to lower case). Encrypt this plaintext using a randomly
generated shift of the alphabet. Remember the key.
"""

# mapping the observation symbols to index
obsSymbols = 'abcdefghijklmnopqrstuvwxyz'
obsSymbols2Index = {}
obsIndex2Symbols = {}
for index,character in enumerate(obsSymbols):
    obsSymbols2Index[character] = index
    obsIndex2Symbols[index] = character
# set maximum T in case read in extra lines from line

def ShiftEncrypt(plainTextIdx):
    plainText = ''.join([obsIndex2Symbols[i] for i in plainTextIdx])
    key = random.randrange(len(obsSymbols2Index))
    cipherTextIdx = [(i + key) % len(obsIndex2Symbols) for i in plainTextIdx]
    cipherText = ''.join([obsIndex2Symbols[i] for i in cipherTextIdx])
    T = len(cipherTextIdx)
    M = len(set(cipherTextIdx))
    print "In py: T=%d" % (T)
    print "In py: M=%d" % (M)
    O = ' '.join([str(x) for x in cipherTextIdx])
    with open("T.out", "w") as f:
        f.write("%d %d" % (T, M))
    print "generate T.out"
    with open("cipherO.out", "w") as f:
        f.write(O)
    print "generate cipherO.out"
    with open("key.out", "w") as f:
        f.write(str(key))
    print "generate key.out"
    print "plainText", 
    print plainText[:10], plainTextIdx[:10]
    print "                     +%d %% 26" % key
    print "cipherText", 
    print cipherText[:10], cipherTextIdx[:10]

def selPartOfText(fileName, startCol, startLine, endLine):
    lines = []
    # sel by row range
    print "Sel Text from Line" + str(startLine) + " to Line" + str(endLine)
    with open(fileName, 'r') as f:
        for line in itertools.islice(f, startLine, endLine):
            lines.append(line[startCol:])
    f.close()
    return lines

def getObsSeq(fileName, startCol, startLine, endLine, maxT): 
    print "get observations sequence...", 
    # read file and select part of text
    lines = selPartOfText(fileName, startCol, startLine, endLine)

    # convert to lower-case and keep only 26 characters and a word space
    obs_seq = []
    for line in lines:
        # remove leading spaces, trimming newline and converting lowercase
        line = (line.lstrip()).rstrip().lower()
        for c in line:
            if c.isalpha(): #if c.isspace() or c.isalpha():
                obs_seq.append(obsSymbols2Index[c]) # convert char to number
    # set maximum T in case read in extra lines from line
    if len(obs_seq) > maxT:
        obs_seq = obs_seq[:maxT]
    return obs_seq

    

def main():
    # make sure "old" files are removed for c not to read "old" ones
    import os
    if os.path.isfile("T.out"):
        os.remove("T.out")
    if os.path.isfile("O.out"):
        os.remove("O.out")
    if os.path.isfile("cipherO.out"):
        os.remove("cipherO.out")
    if os.path.isfile("key.out"):
        os.remove("key.out")

    if len(sys.argv) != 6:
        print """
        USAGE:
        Ch2Q11b.py textFile  startCol  startLine  endLine maxT 
                    [1]       [2]       [3]        [4]     [5] 
        1. textFile:   file name to be read/analyzed (only 1 file)
        2. startCol:   starting column number to be read in
        3. startLine:  starting line number to be read in 
        4. endLine:    ending line number to stop read in 
        5. maxT:       maximum length of observation sequence 
        """
        exit(1)
    else:
        fileName = sys.argv[1]       
        startCol = int(sys.argv[2])
        startLine = int(sys.argv[3])
        endLine = int(sys.argv[4])
        maxT = int(sys.argv[5])

        """
        1. Given
        Observation sequence O = (O_0, O_1, . . . , O_{T-1}).
        """
        plainText = getObsSeq(fileName, startCol, startLine, endLine, maxT)
        ShiftEncrypt(plainText)

if __name__ == '__main__':
    main()

