#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

/**
 * CS271 - Fall2019 
 * hmmCh1Q11.c Inhee Park
 * gcc -o hmmQ10 -Wall -O3 hmmQ10.c
 */
int N; // user input
int T; // will be determined from ObsSeq O[]
int M; // will be determined from len(set(O[]])
int K = 5; // random fluctuation (1/N/K)
int maxT;

float **A; // float A[N][N];
float **B; // float B[N][M];
float *pi; // float pi[N];
int *O; // int O[T];
float *c; // float c[T];
float **alpha; // float alpha[T][N]
float **beta; // float beta[T][N]
float **Gamma; // float Gamma[T][N]
float ***diGamma; // float diGamma[T][N][N]

/**
  set probabilities, p[i] for i = 0, 1, ..., n-1 such that
    0) p[i] ~ p0 +/- f
    1) 0 <= p[i] < 1
    2) sum of p[i] = 1
 */

void set_probabilities(float *p, int n, float p0, float f)
{
    float sum = 0.f;
//  printf("p0 = %f; f = %f\n", p0, f);
    for (int i = 0; i < n; ++i) {
        float r = rand() / (float) RAND_MAX; // [0, RAND_MAX] -> [0, 1]
//      printf("r = %f\n", r);
        r = 2 * f * r - f; // [0, 2f] -> [-f, f]
//      printf("r = %f; f = %f\n", r, f);
//      printf("p0 + r = %.3f + (%.3f)\n", p0, r);
        p[i] = p0 + r;
        sum += p0 + r;
    }
    // normalize
    for (int i = 0; i < n; ++i) {
        p[i] /= sum;
    }
}

void initLambdaModel()
{
    srand(time(NULL));
    float p0 = (float) 1 / N;

    set_probabilities(pi, N, p0, p0 / K);

    for (int i = 0; i < N; ++i) {
        set_probabilities(A[i], N, p0, p0 / K);
    }

    printf("M=%d\n", M);
    //printf("K=%d\n", K);
    float q0 = (float) 1 / M;
    for (int i = 0; i < N; ++i) {
        set_probabilities(B[i], M, q0, q0 / K);
    }
}


void alphaPass(void) {
    c[0] = 0;
    for (int i = 0; i < N; ++i) {
        alpha[0][i] = pi[i] * B[i][O[0]];
        c[0] += alpha[0][i];
    }

    c[0] = 1 / c[0];
    for (int i = 0; i < N; ++i) {
        alpha[0][i] *= c[0];
    }
    for (int t = 1; t < T; ++t) {
        c[t] = 0;
        for (int i = 0; i < N; ++i) {
            alpha[t][i] = 0;
            for (int j = 0; j < N; ++j) {
                alpha[t][i] += alpha[t - 1][j] * A[j][i];
            }
            alpha[t][i] *= B[i][O[t]];
            c[t] += alpha[t][i];
        }
        c[t] = 1 / c[t];
        for (int i = 0; i < N; ++i) {
            alpha[t][i] *= c[t];
        }
    }
}

void betaPass(void) {
    for (int i = 0; i < N; ++i) {
        beta[T - 1][i] = c[T - 1];
    }

    for (int t = T - 2; t >= 0; --t) {
        for (int i = 0; i < N; ++i) {
            beta[t][i] = 0;
            for (int j = 0; j < N; ++j) {
                beta[t][i] += A[i][j] * B[j][O[t + 1]] * beta[t + 1][j];
            }
            beta[t][i] *= c[t];
        }
    }
}

void GammasPass(void) {
    for (int t = 0; t < T - 1; ++t) {
        float denom = 0;
        for (int i = 0; i < N; ++i) {
            for (int j = 0; j < N; ++j) {
                denom += alpha[t][i] * A[i][j] * B[j][O[t + 1]] * beta[t + 1][j];
            }
        }
        for (int i = 0; i < N; ++i) {
            Gamma[t][i] = 0;
            for (int j = 0; j < N; ++j) {
                diGamma[t][i][j] = alpha[t][i] * A[i][j] * B[j][O[t + 1]] * beta[t + 1][j] / denom;
                Gamma[t][i] += diGamma[t][i][j];
            }
        }
    }
    float denom = 0;
    for (int i = 0; i < N; ++i) {
        denom += alpha[T - 1][i];
    }
    for (int i = 0; i < N; ++i) {
        Gamma[T - 1][i] = alpha[T - 1][i] / denom;
    }
}

void reEstimateModel()
{
/*
    // re-estimate pi
    for i = 0 to N-1
        pi_i = gamma_0(i)
    next i
 */
    for (int i = 0; i < N; ++i) {
        pi[i] = Gamma[0][i];
    }
/*
    // re-estimate A and B
    for i = 0 to N-1
        denom = 0
        for t = 0 to T-2
            denom = denom + gamma_t(i)
        next t
        for j = 0 to N-1 // re-estimate A
            numer = 0
            for t = 0 to T-2
                numer = numer + digamma_t(i,j)
            next t
            aij = numer/denom
        next j
        for j = 0 to M-1 // re-estimate B
            numer = 0
            for t = 0 to T-2
                if(O_t == j) then
                    numer = numer + gamma_t(i)
            next t
            bi(j) = numer/denom
        next j
    next i
 */
    for (int i = 0; i < N; ++i) {
        float denom = 0.f;
        for (int t = 0; t < T - 1; ++t) {
            denom += Gamma[t][i];
        }
        for (int j = 0; j < N; ++j) {
            float numer = 0.f;
            for (int t = 0; t < T - 1; ++t) {
                numer += diGamma[t][i][j];
            }
            A[i][j] = numer / denom;
        }
        for (int j = 0; j < M; ++j) {
            float numer = 0.f;
            for (int t = 0; t < T - 1; ++t) {
                if (O[t] == j) {
                    numer += Gamma[t][i];
                }
            }
            B[i][j] = numer / denom;
        }
    }
}

void is_allocated(char var[], void *p)
{
    if (p == NULL) {
        fprintf(stderr, "\"%s\" malloc failed\n", var);
        exit(1);
    }
}

/*
    float A[N][N];
    float B[N][M];
    float pi[N];
    int O[T];
    float c[T];
    float alpha[T][N];
    float beta[T][N];
    float Gamma[T][N];
    float diGamma[T][N][N];
 */
void allocate_memory(void)
{
  A = (float **) malloc(N * sizeof(float *));
  is_allocated("A", A);
  B = (float **) malloc(N * sizeof(float *));
  is_allocated("B", B);
  for (int i = 0; i < N; ++i) {
    A[i] = (float *) malloc(N * sizeof(float));
    is_allocated("A[i]", A[i]);
    B[i] = (float *) malloc(M * sizeof(float));
    is_allocated("B[i]", B[i]);
  }
  pi = (float *) malloc(N * sizeof(float));
  is_allocated("pi", pi);
  c = (float *) malloc(T * sizeof(float));
  is_allocated("c", c);
  O = (int *) malloc(T * sizeof(int));
  is_allocated("O", O);
  alpha = (float **) malloc(T * sizeof(float *));
  is_allocated("alpha", alpha);
  beta = (float **) malloc(T * sizeof(float *));
  is_allocated("beta", beta);
  Gamma = (float **) malloc(T * sizeof(float *));
  is_allocated("Gamma", Gamma);
  diGamma = (float ***) malloc(T * sizeof(float **));
  is_allocated("diGamma", diGamma);
  for (int t = 0; t < T; ++t) {
    alpha[t] = (float *) malloc(N * sizeof(float));
    is_allocated("alpha[t]", alpha[t]);
    beta[t] = (float *) malloc(N * sizeof(float));
    is_allocated("beta[t]", beta[t]);
    Gamma[t] = (float *) malloc(N * sizeof(float));
    is_allocated("Gamma[t]", Gamma[t]);
    diGamma[t] = (float **) malloc(N * sizeof(float *));
    is_allocated("diGamma[t]", diGamma[t]);
    for (int i = 0; i < N; ++i) {
      diGamma[t][i] = (float *) malloc(N * sizeof(float));
      is_allocated("diGamma[t][i]", diGamma[t][i]);
    }
  }
}
void free_memory(void)
{
  for (int i = 0; i < N; ++i) {
    free(A[i]);
    free(B[i]);
  }
  free(A);
  free(B);
  free(c);
  free(pi);
  free(O);
  for (int t = 0; t < T; ++t) {
    for (int i = 0; i < N; ++i) {
      free(diGamma[t][i]);
    }
    free(diGamma[t]);
  }
  free(diGamma);
}

int main(int argc, char *argv[]) {

    int maxIters;
    double logProb, oldLogProb;

    if (argc != 9) {
        printf("USAGE:\n");
        printf("./hmm           fileName   pyFileName  startCol startLine  endLine N    maxIters maxT\n");
        printf("                [1]        [2]          [3]     [4]        [5]     [6]  [7]      [8]\n");
        printf("eg../hmm        corpus_all Ch2Q11a.py   15      0          80000   2    200      50000\n");
        printf("[1] fileName:   file name to be read/analyzed (only 1 file)\n");
        printf("[2] pyFileName: python file name used for text preprocessing\n");
        printf("[3] startCol:   starting column number to be read in\n");
        printf("[4] startLine:  starting line number to be read in\n");
        printf("[5] endLine:    ending line number to stop read in\n");
        printf("[6] N:          number of states   \n");
        printf("[7] maxIters:   maximum number of iteration\n");
        printf("[8] maxT:       maximum length of observation sequence\n\n");
        exit(1);
    } // EO-if
    N = atoi(argv[6]);
    maxIters = atoi(argv[7]);
    maxT= atoi(argv[8]);
    printf("N = %d\n", N);

    ///////////////////////////////
    // 1. Given
    // Observation sequence O = (O_0, O_1, . . . , O_{T-1}).
    ///////////////////////////////
    //  concatenate some of input argument to execute python code for text processing
    //  to obtain an observation sequence in numbers (0, 1, ..., 26)
    char cmd[100] = "python";
    for (int iarg = 1; iarg < 6; ++iarg) {
        if (iarg == 1) {
            strcat(cmd, " ");
            strcat(cmd, argv[iarg+1]);
        } else if (iarg == 2) {
            strcat(cmd, " ");
            strcat(cmd, argv[iarg-1]);
        } else {
            strcat(cmd, " ");
            strcat(cmd, argv[iarg]);
        }
    }
    // maxT
    strcat(cmd, " ");
    strcat(cmd, argv[argc-1]);
    printf("%s\n", cmd);
    if (system(cmd)) {
        fprintf(stderr, "FATAL: check python\n");
        exit(1);
    }
    //
    // read T.out file spitted out from python execution containing T number
    FILE *f;
    f = fopen("T.out", "r");
    if (fscanf(f, "%d", &T) != 1) {
        fprintf(stderr, "FATAL: read error for T\n");
        exit(1);
    }
    if (fscanf(f, "%d", &M) != 1) {
        fprintf(stderr, "FATAL: read error for M\n");
        exit(1);
    }
    printf("T: %d\n", T);
    printf("M: %d\n", M);
    fclose(f);

    // all T, M, N are set, now allocate memories dynamically
    allocate_memory();

    //
    // read O.out file spitted out from python execution observation seq in numbers
    // convert the read-in number sequences to array O
    f = fopen("cipherO.out", "r");
    // int O[T];
    int Oi = 0, n = 0;
    while(fscanf(f, "%d", &Oi) > 0) {
        O[n++] = Oi;
    }
    fclose(f);
    printf("T = %d; n = %d\n", T, n);
    if (n != T) {
        fprintf(stderr, "FATAL: T(%d) set in \"cipherO.out\" is not equal to T(%d) from \"cipherO.out\"\n", T, n);
    }


    ///////////////////////////////
    // 2. Initialize three matrices A, B, and pi.
    //    let pi_i ~ 1/N and let a_ij ~ 1/N and b_j(k) ~ 1/M.
    //    verify the row stochastic conditions
    ///////////////////////////////
    initLambdaModel();
    /*
    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < M; ++j) {
            printf("%.5f ", B[i][j]); 
        }
    }
    */

    ///////////////////////////////
    // 2. Initialize iters = 0; oldLogProb = -inf
    ///////////////////////////////
    int iters = 0;
    logProb = 0.0;
    oldLogProb = -INFINITY;

    while (iters < maxIters) {
        ///////////////////////////////
        // 3. Forward algorithm or alpha-pass
        ///////////////////////////////
        alphaPass();

        ///////////////////////////////
        // 4. Backward algorithm or beta-pass
        ///////////////////////////////
        betaPass();

        ///////////////////////////////
        // 5. Compute the Gammas and di-Gammas
        ///////////////////////////////
        GammasPass();

        ///////////////////////////////
        // 6. Re-estimate the model lambda = (A, B, pi)
        ///////////////////////////////
        reEstimateModel();

        ///////////////////////////////
        // 7. Compute log P(O|lambda)
        ///////////////////////////////
        for (int i = 0; i < T; ++i) {
            logProb += log(c[i]);
        }
        logProb = -logProb;

        logProb = 0.0;
        for (int t = 0; t < T; ++t) {
            logProb += log(c[t]);
        }
        logProb *= -1;

        ///////////////////////////////
        // 8. To iterate or not to iterate, that is the question.
        ///////////////////////////////
        iters += 1;
        printf("iters = %d | %.5f\t(%.5f)\n", iters, logProb, oldLogProb);
        if ((iters < maxIters) || (logProb > oldLogProb)) {
            oldLogProb = logProb;
        } else {
            break;
        }
    } //  EO-while
    printf("--Final B matrix-----\n");
    float B0 = 0.f;
    float B1 = 0.f;
    for (int j = 0; j < M; ++j) {
        /*
        if (j == 0) {
            printf("%2c   %.5f %.5f\n", j, B[0][j], B[1][j]);
        } else {
            printf("%c   %.5f %.5f\n", 96+j, B[0][j], B[1][j]);
        }
        if (fabsf(B[0][j]-B[1][j]) > 0.5E-1) {
            printf("%c  %.5f %.5f **\n", 97+j, B[0][j], B[1][j]);
        } else {
            printf("%c  %.5f %.5f\n", 97+j, B[0][j], B[1][j]);
        }
        */
        printf("%c  %.5f %.5f\n", 97+j, B[0][j], B[1][j]);
        B0 +=  B[0][j];
        B1 +=  B[1][j];
    }
    printf("---------------------\n");
    printf("sum: %.1f\t%.1f\n", B0, B1);

    free_memory();

    return 0;
} // EO-main()
