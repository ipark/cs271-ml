import sys
obsSymbols = 'abcdefghijklmnopqrstuvwxyz'
obsSymbols2Index = {}
obsIndex2Symbols = {}
for index,character in enumerate(obsSymbols):
    obsSymbols2Index[character] = index
    obsIndex2Symbols[index] = character
M = 53
N = 26
IN_LIST = True
if IN_LIST:
    correctMap = list()
    for i in range(N):
        correctMap.append([0] * M)
else:
    correctMap = {}
def zodiac408Decrypt(EncryptFile, DecryptFile):
    Es = []
    with open(EncryptFile, "r") as f:
        for line in f:
            line = line.rstrip()
            Es = [int(x) for x in line.split()]
    print Es
    M = len(Es)
    f.close()

    Ds = []
    with open(DecryptFile, "r") as f:
        for line in f:                          
            line = line.rstrip()
            Ds = [x for i,x in enumerate(line)]
        print Ds
        print len(Ds)
    f.close()

    for i in range(M):
#       correctMap[Ds[i]]=Es[i]
        assert 1 <= Es[i] and Es[i] <= M
        if IN_LIST:
            C = ord(Ds[i])
            assert ord('A') <= C and C <= ord('Z')
            c = C - ord('A')
            correctMap[c][Es[i] - 1] = 1
        else:
            if Ds[i] not in correctMap:
                correctMap[Ds[i]] = set()
            correctMap[Ds[i]].add(Es[i])
    print correctMap

def recoveryPercent(hmmOutput):
    with open(hmmOutput, 'r') as f:
        lines = f.readlines()
    f.close()
    lines = lines[2:-1]
    for i,c in enumerate(obsSymbols):
        if i > 0:
            xs=zip(*[line.split() for line in lines])[i]
            xf=[float(p) for p in xs]
            yp=[q for q in xf if q > 0.1]
            yps=sorted(yp, reverse=True)
            symbols=[]
            if len(yps) > 5:
                yps5 = uniq(yps[:5])
                symbols=','.join([str(xp.index(j) + 1) for j in yps5])
            else:
                symbols=','.join([str(xf.index(j) + 1) for j in yps])
            print str(obsSymbols[i-1]) + "<=" + symbols

            #symbols.append(x.index(y[4])+1) 
            #symbols.append(x.index(y[3])+1) 
            #symbols.append(x.index(y[2])+1) 
            #symbols.append(x.index(y[1])+1) 
            #symbols.append(x.index(y[0])+1)
            #idx = [x.index(m) for m in x[-2:]]
            #print idx
            #for m in sorted(x)[-2:]:
            #    print "%1s <= %2s, %2s" % (obsSymbols[i-1], str(m
            #            str(x.index(max(x)) + 1), obsSymbols[i-1])
            #print 
    #mismatch = [c for i, c in enumerate(putativeKeys) if c is not realKeys[i]]
    #print mismatch
    #print "score:%.4f" % (1.0*(M-len(mismatch))/M)

def main():
    if len(sys.argv) != 4:
        print """
        USAGE:
        matchScore.py  hmmOutputFile  EncryptFile DecryptFile
                       [1]            [2]         [3]
        [1] hmmOutputFile:   transpose B-matrix
        [2] EncryptFile
        [3] DecryptFile
        """
        exit(1)
    else:
        hmmOutput = sys.argv[1]       
        EncryptFile = sys.argv[2]       
        DecryptFile = sys.argv[3]       
        #recoveryPercent(hmmOutput)
        zodiac408Decrypt(EncryptFile, DecryptFile)

if __name__ == '__main__':
    main()

