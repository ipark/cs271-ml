# CS271-Group3-Q2-Skmeans.py
# 
# $ python3 CS271-Group3-Q2-Skmeans.py
# Run this python script inside Code/ dir
# overall directory strcture as following: 
"""
├── Code/clustering4docs/CS271-Group3-Q2-Skmeans.py
└── cs_271_final_data
    ├── CeeInject/*.txt
    ├── Challenge/*.txt
    ├── FakeRean/*.txt
    ├── OnLineGames/*.txt
    ├── Renos/*.txt
    └── Winwebsec/*.txt
"""
import os, glob
import numpy as np
from collections import defaultdict
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import roc_curve, auc
import pandas as pd
#
opcodes20 = ['adc', 'add', 'and', 'cmp', 'dec', 'imul', 
             'inc', 'insl', 'jo', 'js', 'mov', 
             'or', 'out', 'outsl', 'pop', 'push', 
             'sbb', 'sub', 'xchg', 'xor']

familyNames = ["CeeInject", "FakeRean", "OnLineGames", "Renos", "Winwebsec"]

# comma delimited is the default
Cset = pd.read_csv("vectors/CeeInject_1.csv", header=0) 
Fset = pd.read_csv("vectors/FakeRean_1.csv", header=0) 
Oset = pd.read_csv("vectors/OnlineGames_1.csv", header=0) 
Rset = pd.read_csv("vectors/Renos_1.csv", header=0) 
Wset = pd.read_csv("vectors/Winwebsec_1.csv", header=0) 
"""
Cset = pd.DataFrame(np.loadtxt("vectors/CeeInject_vectors.txt"))
Fset = pd.DataFrame(np.loadtxt("vectors/FakeRean_vectors.txt"))
Oset = pd.DataFrame(np.loadtxt("vectors/OnlineGames_vectors.txt"))
Rset = pd.DataFrame(np.loadtxt("vectors/Renos_vectors.txt"))
Wset = pd.DataFrame(np.loadtxt("vectors/Winwebsec_vectors.txt"))
"""

CsetPerSampleNorms = np.linalg.norm(Cset, axis=1).reshape(len(Cset), 1)
NormarlizedCset = Cset #/CsetPerSampleNorms
Ctrain = NormarlizedCset[:800]
Ctest = NormarlizedCset[800:900]
Clabel = pd.DataFrame(['C' for i in range(len(Cset))])

FsetPerSampleNorms = np.linalg.norm(Fset, axis=1).reshape(len(Fset), 1)
NormarlizedFset = Fset #/FsetPerSampleNorms
Ftrain = NormarlizedFset[:800]
Ftest = NormarlizedFset[800:900]
Flabel = pd.DataFrame(['F' for i in range(len(Fset))])

OsetPerSampleNorms = np.linalg.norm(Oset, axis=1).reshape(len(Oset), 1)
NormarlizedOset = Oset #/OsetPerSampleNorms
Otrain = NormarlizedOset[:800]
Otest = NormarlizedOset[800:900]
Olabel = pd.DataFrame(['O' for i in range(len(Oset))])

RsetPerSampleNorms = np.linalg.norm(Rset, axis=1).reshape(len(Rset), 1)
NormarlizedRset = Rset #/RsetPerSampleNorms
Rtrain = NormarlizedRset[:800]
Rtest = NormarlizedRset[800:900]
Rlabel = pd.DataFrame(['R' for i in range(len(Rset))])

WsetPerSampleNorms = np.linalg.norm(Wset, axis=1).reshape(len(Wset), 1)
NormarlizedWset = Wset #/WsetPerSampleNorms
Wtrain = NormarlizedWset[:800]
Wtest = NormarlizedWset[800:900]
Wlabel = pd.DataFrame(['W' for i in range(len(Wset))])
#
NormalizedTrainSet = [Ctrain, Ftrain, Otrain, Rtrain, Wtrain]
NormalizedTestSet = [Ctest, Ftest, Otest, Rtest, Wtest]
#
X_train = pd.concat(NormalizedTrainSet, ignore_index=True)
X_test =  pd.concat(NormalizedTestSet, ignore_index=True)
y_train = pd.concat([Clabel[:800], Flabel[:800], Olabel[:800], Rlabel[:800], Wlabel[:800]])
y_test = pd.concat([Clabel[800:900], Flabel[800:900], Olabel[800:900], Rlabel[800:900], Wlabel[800:900]])
#git clone https://github.com/lovit/clustering4docs.git
from soyclustering import SphericalKMeans

#for myK in [3,4,5,6,7,8,9,10]:
for myK in [10]:
    print("K =",myK) 
    for iter in range(3):
        kmeans = SphericalKMeans(
            n_clusters=myK,
            init='k-means++',
            sparsity=None,
            max_iter=50,
            tol=0.0001,
            verbose = False
        )
        from scipy.sparse import csr_matrix
        TrainSet = np.concatenate((Ctrain, Ftrain, Otrain, Rtrain, Wtrain), axis=0)
        X = csr_matrix((TrainSet))
        y_pred = kmeans.fit_predict(X)
        train_clusters = [l for l in y_pred]
        #centroids = kmeans.cluster_centers_
        
        Y_Ctrain = np.array(train_clusters[:800])
        Y_Ftrain = np.array(train_clusters[800:1600])
        Y_Otrain = np.array(train_clusters[1600:2400])
        Y_Rtrain = np.array(train_clusters[2400:3200])
        Y_Wtrain = np.array(train_clusters[3200:])
        
        clust_str = ', c'.join([str(i) for i in range(myK)])
#       print("\n\nTraining...")
#       print("[K=%d]......  c%s" %  (myK, clust_str))
        clustFamily = {k:[] for k in range(myK)}
        for i, Y in enumerate([Y_Ctrain, Y_Ftrain, Y_Otrain, Y_Rtrain, Y_Wtrain]):
            fDist = np.bincount(Y)
            #print (fDist)
            for j, d in enumerate(fDist):
                clustFamily[j].append(d*100/800)
            Ys = ["%2d" % int(y*100/800) for y in fDist]
#           print("%11s:  %s %% = 100%%" %  (familyNames[i], ', '.join(Ys)))
        Family_clust_pair = defaultdict(list) #{'C':[],'F':[],'O':[],'R':[],'W':[]}
#       print("\nAssining Family to Cluster...")
        for c in clustFamily.keys():
            familyIdx = np.argmax(clustFamily[c])
#           print ("clust%d: %s" % (c, familyNames[familyIdx]))
            f = str(familyNames[familyIdx][0])
            Family_clust_pair[f].append(c)
#       print (Family_clust_pair)


        TestSet = np.concatenate((Ctest, Ftest, Otest, Rtest, Wtest), axis=0)
        Xtest = csr_matrix((TestSet))
        y_pred_test = kmeans.fit_predict(Xtest)
        Y_Ctest = np.array(y_pred_test[:100])
        Y_Ftest = np.array(y_pred_test[100:200])
        Y_Otest = np.array(y_pred_test[200:300])
        Y_Rtest = np.array(y_pred_test[300:400])
        Y_Wtest = np.array(y_pred_test[400:500])
        TPs = 0
        N = 0
        obtainedY = np.zeros((1, 500), dtype=int)[0]
        #{'F': [0, 7], 'R': [1, 3], 'C': [2, 6], 'W': [4], 'O': [5]})
        N = 100 * len(Family_clust_pair.keys())
        if 'C' in Family_clust_pair.keys():
            hit = 0
            for clust in Family_clust_pair['C']:
                hit += np.bincount(Y_Ctest)[clust]
            TPs += hit 
            for i, v in enumerate(Y_Ctest):
                for clust in Family_clust_pair['C']:
                    if v == clust:
                        obtainedY[i] = 1
        if 'F' in Family_clust_pair.keys():
            hit = 0
            for clust in Family_clust_pair['F']:
                hit += np.bincount(Y_Ftest)[clust]
            TPs += hit 
            for i, v in enumerate(Y_Ftest):
                for clust in Family_clust_pair['F']:
                    if v == clust:
                        obtainedY[i+100] = 1
        if 'O' in Family_clust_pair.keys():
            hit = 0
            for clust in Family_clust_pair['O']:
                hit += np.bincount(Y_Otest)[clust]
            TPs += hit 
            for i, v in enumerate(Y_Otest):
                for clust in Family_clust_pair['O']:
                    if v == clust:
                        obtainedY[i+200] = 1
        if 'R' in Family_clust_pair.keys():
            hit = 0
            for clust in Family_clust_pair['R']:
                hit += np.bincount(Y_Rtest)[clust]
            TPs += hit 
            for i, v in enumerate(Y_Rtest):
                for clust in Family_clust_pair['R']:
                    if v == clust:
                        obtainedY[i+300] = 1
        if 'W' in Family_clust_pair.keys():
            hit = 0
            for clust in Family_clust_pair['W']:
                hit += np.bincount(Y_Wtest)[clust]
            TPs += hit 
            for i, v in enumerate(Y_Wtest):
                for clust in Family_clust_pair['W']:
                    if v == clust:
                        obtainedY[i+400] = 1
#       print("\nPrediction with Test Set")
        print("Acc. = %3d/%3d = %.2f | " % (TPs, N, TPs/N), dict(Family_clust_pair))
    

        """
        knownY = np.ones((1, 500), dtype=int)[0]
        #print(obtainedY)
        fpr, tpr, _ = roc_curve(knownY, obtainedY) 
        roc_auc = auc(fpr, tpr)
        plt.plot(fpr,tpr,label='ROC_AUC = %0.2f' % roc_auc)
        
        plt.plot([0, 1], [0, 1], 'k--')
        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.05])
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.legend(loc="lower right")
        plt.title('Spherical K-means (K=5)')
        plt.show()
        """
