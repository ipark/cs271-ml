# CS271-Final-Group3
# 
# $ python3 CS271-Group3-Q1.py 
# Run this python script inside Code/ dir
# overall directory strcture as following: 
"""
├── Code/CS271-Final-Group.py
└── cs_271_final_data
    ├── CeeInject/*.txt
    ├── Challenge/*.txt
    ├── FakeRean/*.txt
    ├── OnLineGames/*.txt
    ├── Renos/*.txt
    └── Winwebsec/*.txt
"""
import os, glob
import numpy as np
from gensim.models import Word2Vec
#
opcodes20 = ['adc', 'add', 'and', 'cmp', 'dec', 'imul', 
             'inc', 'insl', 'jo', 'js', 'mov', 
             'or', 'out', 'outsl', 'pop', 'push', 
             'sbb', 'sub', 'xchg', 'xor']
# Q1a (per family)
"""
familyNames = []
for _ in glob.glob("../cs_271_final_data/Cee*"):
    familyNames.append(_.split('/')[-1])
"""


"""
for family in familyNames:
    PerFamily_Word2Vec = []
    i = 0
    for sample in glob.glob("../cs_271_final_data/%s/*" % family):
        #print(i+1, sample)
        i += 1
        with open(sample) as f:
            PerFamilyTrainSet.extend([_.strip() for _ in f.readlines()])
    print(family, len(PerFamilyTrainSet))
    model = Word2Vec([PerFamilyTrainSet], size=2, window=6, workers=4)
    for i, opcode in enumerate(opcodes20):
        print(model[opcode][0]) print(i+1, model[opcode][0])
"""
    
"""
# Q1b (per sample)
for family in familyNames:
    #with open("%s_900Samples.txt" % family, "w") as g:
        vector = np.zeros((900, 40))
        for i, sample in enumerate(glob.glob("../cs_271_final_data/%s/*.txt" % family)):
            PerSampleWord2Vec = []
            PerSampleTrainSet = []
            with open(sample) as f:
                PerSampleTrainSet.extend([_.strip() for _ in f.readlines()])
            model = Word2Vec([PerSampleTrainSet], size=2, window=6, workers=4)
            vs = np.zeros((20, 2))
            for j, opcode in enumerate(opcodes20):
                if opcode in model.wv.vocab:
                    vs[j] = model.wv[opcode]
            vector[i] = vs.flatten()
        np.savetxt('test.out', vector, delimiter=',', fmt='%.8f')
        #g.write(np.array2string(vector, separator=',', max_line_width=500, suppress_small=True))
         
"""
