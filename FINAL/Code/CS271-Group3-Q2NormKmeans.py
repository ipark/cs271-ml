# CS271-Final-Group3
# 
# $ python3 CS271-Group5.py 
# Run this python script inside Code/ dir
# overall directory strcture as following: 
"""
├── Code/CS271-Group3-Q2NormKmeans.py
└── cs_271_final_data
    ├── CeeInject/*.txt
    ├── Challenge/*.txt
    ├── FakeRean/*.txt
    ├── OnLineGames/*.txt
    ├── Renos/*.txt
    └── Winwebsec/*.txt
"""
import os, glob
import numpy as np
#
opcodes20 = ['adc', 'add', 'and', 'cmp', 'dec', 'imul', 
             'inc', 'insl', 'jo', 'js', 'mov', 
             'or', 'out', 'outsl', 'pop', 'push', 
             'sbb', 'sub', 'xchg', 'xor']

familyNames = ["CeeInject", "FakeRean", "OnLineGames", "Renos", "Winwebsec"]

import pandas as pd
# comma delimited is the default
Cset = pd.read_csv("vectors/CeeInject.csv", header=0) 
CsetPerSampleNorms = np.linalg.norm(Cset, axis=1).reshape(len(Cset), 1)
NormarlizedCset = Cset /CsetPerSampleNorms
Ctrain = NormarlizedCset[:800]
Ctest = NormarlizedCset[800:]
Clabel = pd.DataFrame(['C' for i in range(len(Cset))])

Fset = pd.read_csv("vectors/FakeRean.csv", header=0) 
FsetPerSampleNorms = np.linalg.norm(Fset, axis=1).reshape(len(Fset), 1)
NormarlizedFset = Fset /FsetPerSampleNorms
Ftrain = NormarlizedFset[:800]
Ftest = NormarlizedFset[800:]
Flabel = pd.DataFrame(['F' for i in range(len(Fset))])

Oset = pd.read_csv("vectors/OnlineGames.csv", header=0) 
OsetPerSampleNorms = np.linalg.norm(Oset, axis=1).reshape(len(Oset), 1)
NormarlizedOset = Oset /OsetPerSampleNorms
Otrain = NormarlizedOset[:800]
Otest = NormarlizedOset[800:]
Olabel = pd.DataFrame(['O' for i in range(len(Oset))])

Rset = pd.read_csv("vectors/Renos.csv", header=0) 
RsetPerSampleNorms = np.linalg.norm(Rset, axis=1).reshape(len(Rset), 1)
NormarlizedRset = Rset /RsetPerSampleNorms
Rtrain = NormarlizedRset[:800]
Rtest = NormarlizedRset[800:]
Rlabel = pd.DataFrame(['R' for i in range(len(Rset))])

Wset = pd.read_csv("vectors/Winwebsec.csv", header=0) 
WsetPerSampleNorms = np.linalg.norm(Wset, axis=1).reshape(len(Wset), 1)
NormarlizedWset = Wset /WsetPerSampleNorms
Wtrain = NormarlizedWset[:800]
Wtest = NormarlizedWset[800:]
Wlabel = pd.DataFrame(['W' for i in range(len(Wset))])
#
NormalizedTrainSet = [Ctrain, Ftrain, Otrain, Rtrain, Wtrain]
NormalizedTestSet = [Ctest, Ftest, Otest, Rtest, Wtest]
#
X_train = pd.concat(NormalizedTrainSet, ignore_index=True)
X_test =  pd.concat(NormalizedTestSet, ignore_index=True)
y_train = pd.concat([Clabel[:800], Flabel[:800], Olabel[:800], Rlabel[:800], Wlabel[:800]])
y_test = pd.concat([Clabel[800:], Flabel[800:], Olabel[800:], Rlabel[800:], Wlabel[800:]])

from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
#
for myK in [3, 5, 8, 10]:
    kmeans = KMeans(n_clusters = myK)
    kmeans.fit(X_train)
    y_pred = kmeans.predict(X_test)
    train_clusters = [l for l in kmeans.labels_]
    #print (train_labels)
    #print(kmeans.cluster_centers_)
    #print(y_test)
    #from sklearn.metrics import accuracy_score
    #print(accuracy_score(y_test, y_pred))
    
    Ctrain = np.asarray(train_clusters[:800])
    Ftrain = np.asarray(train_clusters[800:1600])
    Otrain = np.asarray(train_clusters[1600:2400])
    Rtrain = np.asarray(train_clusters[2400:3200])
    Wtrain = np.asarray(train_clusters[3200:])
    
    Ctest = np.asarray(y_pred[:100])
    Ftest = np.asarray(y_pred[100:200])
    Otest = np.asarray(y_pred[200:340])
    Rtest = np.asarray(y_pred[300:420])
    Wtest = np.asarray(y_pred[400:])
    
    clust_str = ', c'.join([str(i) for i in range(myK)]) 
    space_str = ' '*len(clust_str)
    print("\n[K=%d]......  c%s" %  (myK, clust_str))
    for i, Y in enumerate([Ctrain, Ftrain, Otrain, Rtrain, Wtrain]):
        Ys = ["%2d" % int(y) if y>0 else " 0" for y in np.bincount(Y)*100/800]
        print("%11s:  %s %% = 100%%" %  (familyNames[i], ', '.join(Ys)))
    
#import matplotlib.pyplot as plt
#data = clust0
#bins = np.linspace(0, 5, 5)
#plt.xlim([0, 5])
#plt.hist(data, bins=bins)
#plt.show()

