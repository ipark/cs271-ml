#!/usr/bin/env python
# coding: utf-8

# In[40]:


import glob, os
import csv
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
from keras.models import Sequential
from keras.layers.core import Dense
from keras.optimizers import SGD
from keras.utils import to_categorical
from keras.layers import Dropout
import numpy as np
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt


# 
# ## Labels:
# 
# ### 0: CeeInject
# ### 1: FakeRean
# ### 2: OnlineGames
# ### 3: Renos
# ### 4: Winwebsec
# ### 5: Challenge
# 
# 

# In[54]:


labels_train = []
labels_test = []
vectors_train = []
vectors_test = []
num_training_samples = 800


# # Read training vectors

# In[55]:


'''
Reading .csv vectors
'''

# label = 0
# vectors = []
# for fileName in glob.glob('dataset_csv/*.csv'):
#     with open(fileName, 'r') as file:
#         reader = csv.reader(file, quoting=csv.QUOTE_NONNUMERIC)
#         fileVectors = list(reader)
#         i = 0
#         for vectors in fileVectors:
#             if i < num_training_samples:
#                 vectors_train.append(vectors)
#                 labels_train.append(label)
#             else:
#                 vectors_test.append(vectors)
#                 labels_test.append(label)
#             i += 1
#     label += 1

'''
Reading .txt vectors
'''
label = 0
for fileName in glob.glob("dataset_txt/*.txt"):

    # skip over challenge_vectors dataset 
    if fileName == "lda\Challenge_vectors.txt":
        continue

    fileVector = []
    with open(fileName, 'r') as file:
        lines = file.readline()
        lines = lines.replace('[','').split(']')

        i = 0
        for line in lines:
            if line:
                curVector = []
                line = line.split()
                for l in line:
                    curVector.append(float(l))
                if i < num_training_samples:
                    vectors_train.append(curVector)
                    labels_train.append(label)
                else:
                    vectors_test.append(curVector)
                    labels_test.append(label)
                i += 1
        label += 1


# # LDA Classifier

# In[56]:


clf = LDA(solver='lsqr', shrinkage='auto')
clf.fit(vectors_train, labels_train)
score = clf.score(vectors_test, labels_test)
print("Testing accuracy: " + str(score))	# accuracy: 0.752


# # Neural Network Classifier

# In[57]:


def MLP(input_size, 
       hidden1_num_nodes,
       hidden2_num_nodes, 
       hidden3_num_nodes, 
       vectors_train, 
       vectors_test, 
       epochs=100):
   output_num_nodes = 5     # since there are 5 families
   learning_rate = 0.01
   batch_size = 128

   # convert vectors into numpy.array
   train = np.array(vectors_train)
   test = np.array(vectors_test)

   # one hot encode the labels
   train_labels = to_categorical(labels_train)
   test_labels =  to_categorical(labels_test)

   optimizer = SGD(lr=learning_rate)

   model = Sequential()
   model.add(Dense(hidden1_num_nodes, input_shape=(input_size,), activation="tanh"))
   model.add(Dense(hidden2_num_nodes, activation="tanh"))
   model.add(Dense(hidden3_num_nodes, activation="tanh"))
   model.add(Dense(output_num_nodes, activation="softmax"))


   model.compile(loss="categorical_crossentropy", optimizer=optimizer, metrics=["accuracy"])
   model.fit(train, 
         train_labels, 
         validation_data=(test, test_labels), 
         epochs=epochs, 
         batch_size=batch_size)



# In[58]:


input_size = 40           
hidden1_num_nodes = 100   
hidden2_num_nodes = 50   
hidden3_num_nodes = 10  

model = MLP(input_size=input_size, 
            hidden1_num_nodes=hidden1_num_nodes, 
            hidden2_num_nodes=hidden2_num_nodes,
            hidden3_num_nodes=hidden3_num_nodes,
            vectors_train=vectors_train,
            vectors_test= vectors_test)


# # PCA and MLP

# In[59]:


num_comp = 10     
hidden1_num_nodes = 100   
hidden2_num_nodes = 50   
hidden3_num_nodes = 10
epochs = 100

vectors_reduced = np.array(vectors_train + vectors_test)

#ca = PCA().fit(vectors_reduced)
# plt.plot(np.cumsum(pca.explained_variance_ratio_))
# plt.xlabel('number of components')
# plt.ylabel('cumulative explained variance')



pca = PCA(n_components=num_comp)
pca.fit(vectors_reduced)

vectors_reduced = pca.transform(vectors_reduced)
vectors_train_reduced = vectors_reduced[:4000]
vectors_test_reduced = vectors_reduced[4000:]





# In[60]:


# Train MLP using reduced vectors
model = MLP(input_size=num_comp, 
            hidden1_num_nodes=hidden1_num_nodes, 
            hidden2_num_nodes=hidden2_num_nodes,
            hidden3_num_nodes=hidden3_num_nodes,
            vectors_train=vectors_train_reduced, 
            vectors_test=vectors_test_reduced,
            epochs=epochs)


# In[ ]:




